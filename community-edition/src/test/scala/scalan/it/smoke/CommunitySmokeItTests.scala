package scalan.it.smoke

import scalan.community._
import scalan.collections._
import scalan.sql._
import scalan.parrays.{PArraysDslSeq, PArraysDslExp, PArrayExamples}

/**
 *  Tests that very simple examples are run correctly
 */
abstract class CommunitySmokeItTests extends SmokeItTests {

  trait ProgCommunity extends Prog with ScalanCommunity with PArrayExamples with MultiMapsDsl with SqlDsl {

    lazy val simpleConst = fun { x: PA[Int] =>
      PArray.singleton(1)
    }

    //def componentAccess(t: Rep[((Int,Double),(String,Long))]): Rep[String] = t._2._1

    lazy val reuseTest = fun { len: Rep[Int] =>
      val matrix: Rep[Array[Array[Int]]] = Array.tabulate[Array[Int]](len) { n => Array.tabulate[Int](n) { i => i}}
      matrix.map(row => row.reduce) zip matrix.map(row => row.reduce * 2)
    }

    lazy val selectUsingIndex = fun { in: Rep[Array[(Int, Double)]] =>
      val table = Table.create[(Int, Double)]("t1").primaryKey(r => r._1).secondaryKey(r => r._2).insertFrom(in)
      table.where(r => ((r._1 === 3) && (r._2 > 0.0))).select(r => (r._1, r._2 * 2.0)).orderBy(r => r._2)
    }

    lazy val innerJoin = fun { in: Rep[(Array[(Int, Double)], Array[(String, Int)])] =>
      val outer = Table.create[(Int, Double)]("outer").primaryKey(_._1).secondaryKey(_._2).insertFrom(in._1)
      val inner = Table.create[(String, Int)]("inner").primaryKey(_._2).insertFrom(in._2)
      outer.where(r => (r._2 === 1.0)).join(inner)(_._1, _._2).orderBy(_._3)
    }

    lazy val hashJoin = fun { in: Rep[(Array[(Int, Double)], Array[(String, Int)])] =>
      val outer = Table.create[(Int, Double)]("outer").primaryKey(_._1).secondaryKey(_._2).insertFrom(in._1)
      val inner = Table.create[(String, Int)]("inner").primaryKey(_._1).insertFrom(in._2)
      outer.where(r => (r._2 === 1.0)).join(inner)(_._1, _._2).orderBy(_._2)
    }

    lazy val selectCount = fun { in: Rep[Array[(Int, Double)]] =>
      val table = Table.create[(Int, Double)]("t1").primaryKey(_._1).insertFrom(in)
      table.where(_._2 >= 0.0).count
    }

    lazy val groupBy = fun { in: Arr[(Int, Double)] =>
      Table.create[(Int, Double)]("t1").insertFrom(in).groupBy(_._1 % 2).reduce(grp => grp.map(_._2).sum).toArray.sortBy(fun { r => r._1})
    }

    type Record = (Int, String)

    lazy val sqlBenchmark = fun { in: Arr[Record] =>
      val table = Table.create[Record]("t1"). /*primaryKey(r => r._1).*/ insertFrom(in)
      (table.where(r => r._2.contains("123") && r._1 > 0).count,
        table.sum(_._1),
        table.where(_._2.startsWith("1")).sum(_._1))
    }

    lazy val columnarStore = fun { in: Arr[(Int, String)] =>
      val table = PairTable.create(Table.create[Int]("c1"), Table.create[String]("c2")).primaryKey(r => r._1).insertFrom(in)
      table.sum(_._1)
    }

    def doQueries(table: Rep[Table[Record]]) = {
      (table.where(r => r._2.contains("123") && r._1 > 0).count,
        table.where(r => r._2.matches(".*1.*2.*3.*")).count,
        table.sum(_._1),
        table.where(_._2.startsWith("1")).sum(_._1))
    }

    lazy val sqlParBenchmark = fun { in: Arr[Record] =>
      val table = ShardedTable.create[Record]("dist", 4, (node: Rep[Int]) => DatabaseTable.createShard[Record](node), (r: Rep[Record]) => r._1.hashcode).insertFrom(in)
      doQueries(table)
    }

    lazy val sqlColumnarStoreBenchmark = fun { in: Rep[(Int, Array[Record])] =>
      val table = ShardedTable.create[Record]("dist", in._1, (node: Rep[Int]) => PairTable.create[Int,String](DatabaseTable.create[Int](toRep("left") + node.toStr),
                                                                                                          DatabaseTable.create[String](toRep("right") + node.toStr)),
                                              (r: Rep[Record]) => r._1.hashcode).insertFrom(in._2)
      (table.where(r => r._2.contains("123") && r._1 > 0).count,
        table.where(r => r._2.contains("123")).count,
        table.sum(_._1),
        table.where(_._2.startsWith("1")).sum(_._1))
    }


    lazy val sqlIndexBenchmark = fun { in: Arr[Record] =>
      val n_records = in.length
      val table = Table.create[Record]("t1").primaryKey(r => r._1).insertFrom(in)
      loopUntil2(1, 0)(
      { (i, sum) => i > n_records}, { (i, sum) => (i + 1, sum + table.where(_._1 === i).singleton._1)}
      )
    }

    lazy val sqlGroupBy = fun { in: Arr[Record] =>
      val table = ShardedTable.create[Record]("dist", 4, (node: Rep[Int]) => DatabaseTable.createShard[Record](node), (r: Rep[Record]) => r._1.hashcode).insertFrom(in)
      table.groupBy(_._2).reduce(grp => grp.sumBy(fun { r => r._1})).toArray.sortBy(fun { r => r._2})
    }

    lazy val sqlMapReduce = fun { in: Rep[(Int, Array[Record])] =>
      val table = ShardedTable.create[Record]("dist", in._1, (node: Rep[Int]) => DatabaseTable.createShard[Record](node), (r: Rep[Record]) => r._1.hashcode).insertFrom(in._2)
      table.mapReduce[String, Int](r => (r._2, r._1), p => p._1 + p._2).toArray.sortBy(fun { r => r._2})
    }

    lazy val sqlParallelJoin = fun { in: Rep[(Array[Record], (Array[Record], Array[Record]))] =>
      val t1 = ShardedTable.create[Record]("t1", 4, (node: Rep[Int]) => DatabaseTable.createShard[Record](node).primaryKey(_._1), (r: Rep[Record]) => r._1.hashcode).insertFrom(in._1)
      val t2 = ShardedTable.create[Record]("t2", 4, (node: Rep[Int]) => DatabaseTable.createShard[Record](node).primaryKey(_._1), (r: Rep[Record]) => r._1.hashcode).insertFrom(in._2)
      val t3 = ShardedTable.create[Record]("t3", 4, (node: Rep[Int]) => DatabaseTable.createShard[Record](node).primaryKey(_._1), (r: Rep[Record]) => r._1.hashcode).insertFrom(in._3)
      /*
      (t1.join(t2)(_._1, _._1).join(t3)(_._1._1, _._1).singleton,
       t1.where(r => r._1 === 2).join(t2)(_._1, _._1).join(t3)(_._1._1, _._1).singleton,
       t1.where(r => r._2 matches("1.*")).join(t2)(_._1, _._1).join(t3)(_._1._1, _._1).select(r => (r._1._1._1, r._1._1._2 + "-" + r._1._2._2 + "-" + r._2._2)))
*/
      (t1.join(t2)(_.head, _.head).join(t3)(_.head.head, _.head).singleton,
        t1.where(r => r.head === 2).join(t2)(_.head, _.head).join(t3)(_.head.head, _.head).singleton,
        t1.where(r => r.tail matches (".*ee.*")).join(t2)(_.head, _.head).join(t3)(_.head.head, _.head).select(r => (r.head.head.head, r.head.head.tail + "-" + r.head.tail.tail + "-" + r.tail.tail)).singleton)
    }

    lazy val sqlIndexJoin = fun { in: Rep[(Array[Record], Array[Record])] =>
      val outer = Table.create[Record]("outer").primaryKey(_.head).insertFrom(in.head)
      val inner = Table.create[Record]("inner").secondaryKey(_.head).insertFrom(in.tail)
      outer.join(inner)(_.head, _.head).orderBy(_.tail.tail)
    }


    type Relation = (Int, Int)

    lazy val sqlAggJoin = fun { in: Rep[(Array[Relation], (Array[Relation], (Array[Relation], (Array[Relation], (Array[Relation], Int)))))] =>
      val t1 = ShardedTable.create[Relation]("t1", in._6, (node: Rep[Int]) => DatabaseTable.createShard[Relation](node), (r: Rep[Relation]) => r._1.hashcode).insertFrom(in._1)
      val t2 = DatabaseTable.create[Relation]("t2").primaryKey(_._1).insertFrom(in._2)
      val t3 = DatabaseTable.create[Relation]("t3").primaryKey(_._1).insertFrom(in._3)
      val t4 = DatabaseTable.create[Relation]("t4").primaryKey(_._1).insertFrom(in._4)
      val t5 = DatabaseTable.create[Relation]("t5").primaryKey(_._1).insertFrom(in._5)
      t1.where(r => r.head % 3 === 0).join(t2)(_.tail, _.head).join(t3)(_.tail.tail, _.head).join(t4)(_.tail.tail, _.head).join(t5)(_.tail.tail, _.head).where(r => r.tail.tail % 3 === 0).sum(_.tail.tail)
    }

    lazy val testTuple = fun { in: Rep[(Array[Record], Array[Record])] =>
      in._1.zip(in._2).map(_.head.head)
    }

    lazy val ifTest = fun { in: Rep[(Int, Double)] =>
      val map = PMap.empty[Int, Double]
      IF(map.contains(in._1)) THEN {
        THROW("Key already exists")
      } ELSE {
        map.update(in._1, in._2)
      }
    }

    lazy val unionMaps = fun { in: Rep[(Array[(Int, Double)], Array[(Int, Double)])] =>
      val map1 = PMap.fromArray[Int, Double](in._1)
      val map2 = PMap.fromArray[Int, Double](in._2)
      map1.union(map2).toArray.sort
    }
    lazy val differenceMaps = fun { in: Rep[(Array[(Int, Double)], Array[(Int, Double)])] =>
      val map1 = PMap.fromArray[Int, Double](in._1)
      val map2 = PMap.fromArray[Int, Double](in._2)
      map1.difference(map2).toArray.sort
    }
    lazy val joinMaps = fun { in: Rep[(Array[(Int, Double)], Array[(Int, Double)])] =>
      val map1 = PMap.fromArray[Int, Double](in._1)
      val map2 = PMap.fromArray[Int, Double](in._2)
      map1.join(map2).toArray.sort
    }
    lazy val reduceMaps = fun { in: Rep[(Array[(Int, Double)], Array[(Int, Double)])] =>
      val map1 = PMap.fromArray[Int, Double](in._1)
      val map2 = PMap.fromArray[Int, Double](in._2)
      map1.reduce(map2, fun2 { (a, b) => a + b}).toArray.sort
    }
    lazy val iterateMap = fun { in: Rep[Array[(Int, Double)]] =>
      val map = PMap.fromArray[Int, Double](in)
      loopUntil2(1, 0.0)(
      { (i, sum) => (!map.contains(i) && i > map.size)}, { (i, sum) => (i + 1, sum + map(i))}
      )
    }
    lazy val mapReduceByKey = fun { in: Rep[Array[Int]] =>
      in.mapReduce[Int, Int](fun { a => (a, toRep(1))}, fun2 { (a, b) => a + b}).toArray.sort
    }
    lazy val filterCompound = fun { in: Rep[Array[(Int, (Int, Int))]] =>
      in.filter(x => x._1 >= 20 && x._2 >= x._1 && x._3 < 30)
    }
    /*
    lazy val filterCompoundPArray = fun {in: Rep[Array[(Int, (Int, Int))]] =>
      val pa = PArray(in)
      pa.filter(x => x._1 >= 20 && x._2 >= x._1 && x._3 < 30)
    }
    */
    lazy val aggregates = fun { in: Rep[Array[Int]] =>
      (in.min, in.max, in.sum, in.avg)
    }
    lazy val sortBy = fun { in: Rep[Array[(Int, Int)]] =>
      in.sortBy(fun { p => p._1})
    }
    lazy val groupByCount = fun { in: Rep[Array[(Int, Int)]] =>
      in.groupBy(fun { p => p._1}).mapValues(g => g.length).toArray.sortBy(fun { p => p._1})
    }
    lazy val groupBySum = fun { in: Rep[Array[(Int, Int)]] =>
      in.groupBy(fun { p => p._1}).mapValues(g => g.toArray.map(p => p._2).sum).toArray.sortBy(fun { p => p._1})
    }
    lazy val compoundMapKey = fun { in: Rep[(Array[(Int, Double)], Array[Int])] =>
      val map = PMap.fromArray[(Int, Double), Int](in._1 zip in._2)
      loopUntil2(0, 0)(
      { (i, sum) => (i >= map.size)}, { (i, sum) => (i + 1, sum + map(in._1(i)))}
      )
    }
    lazy val compoundMapValue = fun { in: Rep[(Array[String], Array[(Int, Double)])] =>
      val map = PMap.fromArray[String, (Int, Double)](in._1 zip in._2)
      map("two")._2
    }
    lazy val fillArrayBuffer = fun { in: Rep[Array[Int]] =>
      in.fold(ArrayBuffer.empty[Int], (state: Rep[ArrayBuffer[Int]], x: Rep[Int]) => state += x).toArray
    }
    lazy val unionMultiMaps = fun { in: Rep[(Array[(Int, Double)], Array[(Int, Double)])] =>
      val map1 = MultiMap.fromArray[Int, Double](in._1)
      val map2 = MultiMap.fromArray[Int, Double](in._2)
      map1.union(map2).toArray.map(p => (p._1, p._2.toArray.sum)).sortBy(fun { p => p._1})
    }

    /*
    lazy val filterUDT = fun {in: Rep[Array[(Int, (Int, Int))]] =>
      in.map(x => Triple(x._1, x._2, x._3)).filter(t => t.entity >= 20 && t.attribute >= t.entity && t.value < 30)
    }
*/
  }

class ProgCommunitySeq extends ProgCommunity with PArraysDslSeq with ScalanCommunityDslSeq with MultiMapsDslSeq with SqlDslSeq {
}

// TODO
//  override val progStaged: ProgCommunity with PArraysDslExp with ScalanCommunityExp with Compiler
//  override val progSeq = new ProgCommunitySeq()

  //  test("test00simpleConst") {
//    val (in, out) = Array(0) -> Array(1)
//    progSeq.simpleConst(progSeq.PArray.fromArray(in)).arr should be(out)
//    checkRun(progSeq, progStaged)(progSeq.simpleConst, progStaged.simpleConst)("00simpleConst", progSeq.PArray.fromArray(in), progSeq.PArray.fromArray(out))
//  }

//
//  @Test def test01simpleMap {
//    val (in, out) = Array(1, 2, 3, 4, 5) -> Array(2, 3, 4, 5, 6)
//    Assert.assertArrayEquals(out, progSeq.simpleMap(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleMap, progStaged.simpleMap)("01simpleMap", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//
//  @Test def test02simpleLoop {
//    val (in, out) = Array(1, 2, 3, 4, 5) -> Array(15)
//    Assert.assertArrayEquals(out, progSeq.simpleLoop(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleLoop, progStaged.simpleLoop)("02simpleLoop", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test03simpleFirstElement {
//    val (in, out) = Array(3, 4) -> Array(3)
//    Assert.assertArrayEquals(out, progSeq.simpleFirstElement(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleFirstElement, progStaged.simpleFirstElement)("03simpleFirstElement", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//
//  @Test def test04simpleLoop {
//    val (in, out) = Array(1, 2, 3, 4) -> Array(4)
//    Assert.assertArrayEquals(out, progSeq.simpleLoop2(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleLoop2, progStaged.simpleLoop2)("04simpleLoop", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // fails since tabulate is not implemented in staging
//  @Test def test05simpleAppend {
//    val (in, out) = Array(1, 2) -> Array(1, 2, 1, 2)
//    Assert.assertArrayEquals(out, progSeq.simpleAppend(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleAppend, progStaged.simpleAppend)("05simpleAppend", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test06simpleAppend {
//    val (in, out) = Array(1, 2) -> Array(1, 2, 1, 2)
//    Assert.assertArrayEquals(out, progSeq.simpleAppend2(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleAppend2, progStaged.simpleAppend2)("06simpleAppend", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test07simpleId {
//    val (in, out) = Array(1, 2) -> Array(1, 2)
//    Assert.assertArrayEquals(out, progSeq.idPA(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.idPA, progStaged.idPA)("07simpleId", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test08curredPlusMap {
//    val (in, out) = Array(1, 2) -> Array(2, 4)
//    Assert.assertArrayEquals(out, progSeq.curredPlusMap(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.curredPlusMap, progStaged.curredPlusMap)("08curredPlusMap", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test09filterEven {
//    val (in, out) = Array(1, 2, 3, 4, 5, 6) -> Array(2, 4, 6)
//    Assert.assertArrayEquals(out, progSeq.filterEven(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.filterEven, progStaged.filterEven)("09filterEven", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // fails - currently signed integers are not supported by runtime
//  @Test def test10filterPositive {
//    val (in, out) = Array(-1, -2, 0, 1, 2, -3, 3) -> Array(1,2,3)
//    Assert.assertArrayEquals(out, progSeq.filterPositive(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.filterPositive, progStaged.filterPositive)("10filterPositive", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // fails - currently it is impossible to use your own monoid
//  @Test def test11reduceMonoid {
//    val (in, out) = Array(1, 2, 2) -> Array(1)
//    Assert.assertArrayEquals(out, progSeq.div2Reduce(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.div2Reduce, progStaged.div2Reduce)("11div2Reduce", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test12for {
//    val (in, out) = Array(1, 2, 3, 4, 5) -> Array(3, 4, 5)
//    Assert.assertArrayEquals(out, progSeq.simpleFor(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.simpleFor, progStaged.simpleFor)("12for", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def test13reverse {
//    val (in, out) = Array(1, 2, 3, 4, 5) -> Array(5, 4, 3, 2, 1)
//    Assert.assertArrayEquals(out, progSeq.reverse(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.reverse, progStaged.reverse)("13reverse", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // fails for staging
//  @Test def test14toFromArray {
//    val (in, out) = Array(1, 2, 3, 4, 5) -> Array(1, 2, 3, 4, 5)
//    Assert.assertArrayEquals(out, progSeq.toFromArray(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.toFromArray, progStaged.toFromArray)("14toFromArray", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def simple15flatMap1 {
//    val (in, out) = Array(1, 2, 3) -> Array(1, 2, 2, 3, 3, 3)
//    Assert.assertArrayEquals(out, progSeq.flatMap1(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.flatMap1, progStaged.flatMap1)("15flatMap", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // compiled cpp program fails
//  @Test def simple16flatMap2 {
//    val (in, out) = Array(1, 2) -> Array(1, 2, 2, 2, 2)
//    Assert.assertArrayEquals(out, progSeq.flatMap2(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.flatMap2, progStaged.flatMap2)("16flatMap", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  // compiled cpp program fails
//  @Test def simple17flatMap2 {
//    val (in, out) = Array(1, 2, 3) -> Array(1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3)
//    Assert.assertArrayEquals(out, progSeq.flatMap2(progSeq.fromArray(in)).toArray)
//    checkRun(progSeq, progStaged)(progSeq.flatMap2, progStaged.flatMap2)("17flatMap", progSeq.fromArray(in), progSeq.fromArray(out))
//  }
//
//  @Test def simple18pairToArray {
//    val (in, out) = intPair -> Array(1, 2)
//    Assert.assertArrayEquals(out, progSeq.pairToArray(in).toArray)
//    checkRun(progSeq, progStaged)(progSeq.pairToArray, progStaged.pairToArray)("18pairToArray", in, progSeq.fromArray(out))
//  }
//
//  @Test def simple19valuesOfNA {
//    val (in, out) = nestedArray1 -> Array(1, 2, 3, 4, 5, 6)
//    Assert.assertArrayEquals(out, progSeq.valuesOfNA(in).toArray)
//    checkRun(progSeq, progStaged)(progSeq.valuesOfNA, progStaged.valuesOfNA)("19valuesOfNA", in, progSeq.fromArray(out))
//  }
//
//  @Test def simple20valuesOfNA2 {
//    val (in, out) = nestedArray3 -> Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
//    Assert.assertArrayEquals(out, progSeq.valuesOfNA2(in).toArray)
//    checkRun(progSeq, progStaged)(valuesOfNA2, progStaged.valuesOfNA2)("20valuesOfNA2", in, progSeq.fromArray(out))
//  }
//
//  @Test def simple21simpleLog {
//    val (in, out) = in1 -> out1
//    Assert.assertEquals(out._1, progSeq.logTest(in)._1, 0.01f)
//    Assert.assertEquals(out._2, progSeq.logTest(in)._2, 0.01f)
//    val stagedOut = testRun(progSeq, progStaged)(logTest, progStaged.logTest)("21simpleLog", in)
//    Assert.assertEquals(out._1, stagedOut._1, 0.01f)
//    Assert.assertEquals(out._2, stagedOut._2, 0.01f)
//  }
//
//  @Test def simple22doubleSwap {
//    val (in, out) = in2 -> out2
//    Assert.assertEquals(out._1, progSeq.doubleSwap(in)._1, 0.01)
//    Assert.assertEquals(out._2, progSeq.doubleSwap(in)._2, 0.01)
//    val stagedOut = testRun(progSeq, progStaged)(doubleSwap, progStaged.doubleSwap)("22doubleSwap", in)
//    Assert.assertEquals(out._1, stagedOut._1, 0.01)
//    Assert.assertEquals(out._2, stagedOut._2, 0.01)
//  }
//
//  @Test def simple23sMatMul {
//    val (in, out) = (smdv, null)
//    progSeq.sMatDVecMul(in)
//    val stagedOut = testRun(progSeq, progStaged)(sMatDVecMul, progStaged.sMatDVecMul)("23matMul", in)
//    //Assert.assertEquals(out._1, stagedOut._1, 0.01)
//    //Assert.assertEquals(out._2, stagedOut._2, 0.01)
//  }
//
//  @Test def simple24fusion {
//    val (in, out) = (pairsOfArrays, null)
//    progSeq.fusion(in)
//    val stagedOut = testRun(progSeq, progStaged)(fusion, progStaged.fusion)("24fusion", in)
//    //Assert.assertEquals(out._1, stagedOut._1, 0.01)
//    //Assert.assertEquals(out._2, stagedOut._2, 0.01)
//  }
//
//  @Test def simple25fusion {
//    val (in, out) = (intArray, null)
//    progSeq.fusion1(in)
//    val stagedOut = testRun(progSeq, progStaged)(fusion1, progStaged.fusion1)("25fusion", in)
//    //Assert.assertEquals(out._1, stagedOut._1, 0.01)
//    //Assert.assertEquals(out._2, stagedOut._2, 0.01)
//  }
//
////  @Test def simple26_expandScaledRanges {
////    val in = Pair(fromArray(Array(10, 20, 30)), 2)
////    val out = Array(20, 21, 40, 41, 60, 61)
////    Assert.assertArrayEquals(out, progSeq.expandScaledRangesFun(in).toArray)
////    checkRun(progSeq, progStaged)(expandScaledRangesFun, progStaged.expandScaledRangesFun)("simple26_expandScaledRanges", in, progSeq.fromArray(out))
////  }

  override val progSeq: ProgCommunitySeq = new ProgCommunitySeq
}
