package scalan.udt

import scalan._
import scalan.common.Default
import scalan.common.Lazy
import scalan.common.OverloadHack.Overloaded1
/*
trait Triples extends Base { self: TriplesDsl =>
  type PT[E,A,V] = Rep[Triple[E,A,V]]

  trait Triple[E,A,V] extends Reifiable[Triple[E,A,V]] {
    def entity: Rep[E]
    def attribute: Rep[A]
    def value: Rep[V]
  }

  trait TripleCompanion extends TypeFamily3[Triple] {
    def defaultOf[E:Elem,A:Elem,V:Elem]: Default[Rep[Triple[E,A,V]]] =
      TupleTriple.defaultOf[E,A,V]
    def apply[E:Elem,A:Elem,V:Elem](e: Rep[E], a: Rep[A], v: Rep[V]): PT[E,A,V] = TupleTriple(e, a, v)
  }
  
  abstract class TupleTriple[E,A,V](val entity: Rep[E], val attribute: Rep[A], val value: Rep[V])
    (implicit val eEnity: Elem[E], val eAttribute: Elem[A], val eValue: Elem[V]) extends Triple[E,A,V] {
  }
     
  trait TupleTripleCompanion extends ConcreteClass3[TupleTriple] with TripleCompanion {
    override def defaultOf[E:Elem,A:Elem,V:Elem] =
      Default.defaultVal(TupleTriple(element[E].defaultRepValue, element[A].defaultRepValue, element[V].defaultRepValue))
    def apply[E:Elem,A:Elem,V:Elem](e: Rep[E], a: Rep[A], v: Rep[V]): PT[E,A,V] = TupleTriple(e, a, v)
  }
}

trait TriplesDsl extends ScalanDsl with impl.TriplesAbs with Triples with Scalan

trait TriplesDslSeq extends TriplesDsl with impl.TriplesSeq with ScalanSeq

trait TriplesDslExp extends TriplesDsl with impl.TriplesExp with ScalanExp
*/