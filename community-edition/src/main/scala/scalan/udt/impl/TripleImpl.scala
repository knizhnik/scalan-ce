package scalan.udt
package impl

import scalan._
import scalan.common.Default
import scalan.common.Lazy
import scalan.common.OverloadHack.Overloaded1
import scala.reflect.runtime.universe._
import scalan.common.Default
/*
trait TriplesAbs extends Triples
{ self: TriplesDsl =>
  // single proxy for each type family
  implicit def proxyTriple[E, A, V](p: Rep[Triple[E, A, V]]): Triple[E, A, V] =
    proxyOps[Triple[E, A, V]](p)

  abstract class TripleElem[E, A, V, From, To <: Triple[E, A, V]](iso: Iso[From, To]) extends ViewElem[From, To]()(iso)

  trait TripleCompanionElem extends CompanionElem[TripleCompanionAbs]
  implicit lazy val TripleCompanionElem: TripleCompanionElem = new TripleCompanionElem {
    lazy val tag = typeTag[TripleCompanionAbs]
    lazy val defaultRep = Default.defaultVal(Triple)
  }

  trait TripleCompanionAbs extends TripleCompanion {
    override def toString = "Triple"
  }
  def Triple: Rep[TripleCompanionAbs]
  implicit def proxyTripleCompanion(p: Rep[TripleCompanion]): TripleCompanion = {
    proxyOps[TripleCompanion](p)
  }

  // elem for concrete class
  class TupleTripleElem[E, A, V](iso: Iso[TupleTripleData[E, A, V], TupleTriple[E, A, V]]) extends TripleElem[E, A, V, TupleTripleData[E, A, V], TupleTriple[E, A, V]](iso)

  // state representation type
  type TupleTripleData[E, A, V] = (E, (A, V))

  // 3) Iso for concrete class
  class TupleTripleIso[E, A, V](implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V])
    extends Iso[TupleTripleData[E, A, V], TupleTriple[E, A, V]] {
    override def from(p: Rep[TupleTriple[E, A, V]]) =
      unmkTupleTriple(p) match {
        case Some((entity, attribute, value)) => Pair(entity, Pair(attribute, value))
        case None => !!!
      }
    override def to(p: Rep[(E, (A, V))]) = {
      val Pair(entity, Pair(attribute, value)) = p
      TupleTriple(entity, attribute, value)
    }
    lazy val tag = {
      implicit val tagE = element[E].tag
      implicit val tagA = element[A].tag
      implicit val tagV = element[V].tag
      typeTag[TupleTriple[E, A, V]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[TupleTriple[E, A, V]]](TupleTriple(element[E].defaultRepValue, element[A].defaultRepValue, element[V].defaultRepValue))
    lazy val eTo = new TupleTripleElem[E, A, V](this)
  }
  // 4) constructor and deconstructor
  trait TupleTripleCompanionAbs extends TupleTripleCompanion {
    override def toString = "TupleTriple"
    def apply[E, A, V](p: Rep[TupleTripleData[E, A, V]])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]): Rep[TupleTriple[E, A, V]] =
      isoTupleTriple(eEnity, eAttribute, eValue).to(p)
    def apply[E, A, V](entity: Rep[E], attribute: Rep[A], value: Rep[V])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]): Rep[TupleTriple[E, A, V]] =
      mkTupleTriple(entity, attribute, value)
    def unapply[E:Elem, A:Elem, V:Elem](p: Rep[TupleTriple[E, A, V]]) = unmkTupleTriple(p)
  }
  def TupleTriple: Rep[TupleTripleCompanionAbs]
  implicit def proxyTupleTripleCompanion(p: Rep[TupleTripleCompanionAbs]): TupleTripleCompanionAbs = {
    proxyOps[TupleTripleCompanionAbs](p)
  }

  class TupleTripleCompanionElem extends CompanionElem[TupleTripleCompanionAbs] {
    lazy val tag = typeTag[TupleTripleCompanionAbs]
    lazy val defaultRep = Default.defaultVal(TupleTriple)
  }
  implicit lazy val TupleTripleCompanionElem: TupleTripleCompanionElem = new TupleTripleCompanionElem

  implicit def proxyTupleTriple[E:Elem, A:Elem, V:Elem](p: Rep[TupleTriple[E, A, V]]): TupleTriple[E, A, V] =
    proxyOps[TupleTriple[E, A, V]](p)

  implicit class ExtendedTupleTriple[E, A, V](p: Rep[TupleTriple[E, A, V]])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]) {
    def toData: Rep[TupleTripleData[E, A, V]] = isoTupleTriple(eEnity, eAttribute, eValue).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoTupleTriple[E, A, V](implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]): Iso[TupleTripleData[E, A, V], TupleTriple[E, A, V]] =
    new TupleTripleIso[E, A, V]

  // 6) smart constructor and deconstructor
  def mkTupleTriple[E, A, V](entity: Rep[E], attribute: Rep[A], value: Rep[V])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]): Rep[TupleTriple[E, A, V]]
  def unmkTupleTriple[E:Elem, A:Elem, V:Elem](p: Rep[TupleTriple[E, A, V]]): Option[(Rep[E], Rep[A], Rep[V])]
}

trait TriplesSeq extends TriplesAbs { self: ScalanSeq with TriplesDsl =>
  lazy val Triple: Rep[TripleCompanionAbs] = new TripleCompanionAbs with UserTypeSeq[TripleCompanionAbs, TripleCompanionAbs] {
    lazy val selfType = element[TripleCompanionAbs]
  }

  case class SeqTupleTriple[E, A, V]
      (override val entity: Rep[E], override val attribute: Rep[A], override val value: Rep[V])
      (implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V])
    extends TupleTriple[E, A, V](entity, attribute, value) with UserTypeSeq[Triple[E,A,V], TupleTriple[E, A, V]] {
    lazy val selfType = element[TupleTriple[E, A, V]].asInstanceOf[Elem[Triple[E,A,V]]]
  }
  lazy val TupleTriple = new TupleTripleCompanionAbs with UserTypeSeq[TupleTripleCompanionAbs, TupleTripleCompanionAbs] {
    lazy val selfType = element[TupleTripleCompanionAbs]
  }

  def mkTupleTriple[E, A, V]
      (entity: Rep[E], attribute: Rep[A], value: Rep[V])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]) =
      new SeqTupleTriple[E, A, V](entity, attribute, value)
  def unmkTupleTriple[E:Elem, A:Elem, V:Elem](p: Rep[TupleTriple[E, A, V]]) =
    Some((p.entity, p.attribute, p.value))
}

trait TriplesExp extends TriplesAbs { self: ScalanExp with TriplesDsl =>
  lazy val Triple: Rep[TripleCompanionAbs] = new TripleCompanionAbs with UserTypeDef[TripleCompanionAbs, TripleCompanionAbs] {
    lazy val selfType = element[TripleCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  case class ExpTupleTriple[E, A, V]
      (override val entity: Rep[E], override val attribute: Rep[A], override val value: Rep[V])
      (implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V])
    extends TupleTriple[E, A, V](entity, attribute, value) with UserTypeDef[Triple[E,A,V], TupleTriple[E, A, V]] {
    lazy val selfType = element[TupleTriple[E, A, V]].asInstanceOf[Elem[Triple[E,A,V]]]
    override def mirror(t: Transformer) = ExpTupleTriple[E, A, V](t(entity), t(attribute), t(value))
  }

  lazy val TupleTriple: Rep[TupleTripleCompanionAbs] = new TupleTripleCompanionAbs with UserTypeDef[TupleTripleCompanionAbs, TupleTripleCompanionAbs] {
    lazy val selfType = element[TupleTripleCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object TupleTripleMethods {

  }

  object TupleTripleCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[TupleTripleCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object apply {
      def unapply(d: Def[_]): Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, Seq(e, a, v, _*)) if method.getName == "apply" && receiver.elem.isInstanceOf[TupleTripleCompanionElem] =>
          Some((e, a, v)).asInstanceOf[Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkTupleTriple[E, A, V]
    (entity: Rep[E], attribute: Rep[A], value: Rep[V])(implicit eEnity: Elem[E], eAttribute: Elem[A], eValue: Elem[V]) =
    new ExpTupleTriple[E, A, V](entity, attribute, value)
  def unmkTupleTriple[E:Elem, A:Elem, V:Elem](p: Rep[TupleTriple[E, A, V]]) =
    Some((p.entity, p.attribute, p.value))

  object TripleMethods {
    object entity {
      def unapply(d: Def[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "entity" && receiver.elem.isInstanceOf[TripleElem[_, _, _, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object attribute {
      def unapply(d: Def[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "attribute" && receiver.elem.isInstanceOf[TripleElem[_, _, _, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object value {
      def unapply(d: Def[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "value" && receiver.elem.isInstanceOf[TripleElem[_, _, _, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Triple[E, A, V]] forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object TripleCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[TripleCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object apply {
      def unapply(d: Def[_]): Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}] = d match {
        case MethodCall(receiver, method, Seq(e, a, v, _*)) if method.getName == "apply" && receiver.elem.isInstanceOf[TripleCompanionElem] =>
          Some((e, a, v)).asInstanceOf[Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[E], Rep[A], Rep[V]) forSome {type E; type A; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }
}
*/