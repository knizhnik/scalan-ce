package scalan.sql
package impl

import scalan._
import scalan.collections._
import scalan.common.Default
import scala.reflect.runtime.universe._
import scalan.common.Default

trait SqlAbs extends Sql
{ self: SqlDsl =>
  // single proxy for each type family
  implicit def proxyTable[R](p: Rep[Table[R]]): Table[R] =
    proxyOps[Table[R]](p)

  abstract class TableElem[R, From, To <: Table[R]](iso: Iso[From, To]) extends ViewElem[From, To]()(iso)

  trait TableCompanionElem extends CompanionElem[TableCompanionAbs]
  implicit lazy val TableCompanionElem: TableCompanionElem = new TableCompanionElem {
    lazy val tag = typeTag[TableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(Table)
  }

  trait TableCompanionAbs extends TableCompanion {
    override def toString = "Table"
  }
  def Table: Rep[TableCompanionAbs]
  implicit def proxyTableCompanion(p: Rep[TableCompanion]): TableCompanion = {
    proxyOps[TableCompanion](p)
  }

  // elem for concrete class
  class BaseTableElem[R](iso: Iso[BaseTableData[R], BaseTable[R]]) extends TableElem[R, BaseTableData[R], BaseTable[R]](iso)

  // state representation type
  type BaseTableData[R] = String

  // 3) Iso for concrete class
  class BaseTableIso[R](implicit schema: Elem[R])
    extends Iso[BaseTableData[R], BaseTable[R]] {
    override def from(p: Rep[BaseTable[R]]) =
      unmkBaseTable(p) match {
        case Some((tableName)) => tableName
        case None => !!!
      }
    override def to(p: Rep[String]) = {
      val tableName = p
      BaseTable(tableName)
    }
    lazy val tag = {
      implicit val tagR = element[R].tag
      typeTag[BaseTable[R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[BaseTable[R]]](BaseTable(""))
    lazy val eTo = new BaseTableElem[R](this)
  }
  // 4) constructor and deconstructor
  trait BaseTableCompanionAbs extends BaseTableCompanion {
    override def toString = "BaseTable"

    def apply[R](tableName: Rep[String])(implicit schema: Elem[R]): Rep[BaseTable[R]] =
      mkBaseTable(tableName)
    def unapply[R:Elem](p: Rep[BaseTable[R]]) = unmkBaseTable(p)
  }
  def BaseTable: Rep[BaseTableCompanionAbs]
  implicit def proxyBaseTableCompanion(p: Rep[BaseTableCompanionAbs]): BaseTableCompanionAbs = {
    proxyOps[BaseTableCompanionAbs](p)
  }

  class BaseTableCompanionElem extends CompanionElem[BaseTableCompanionAbs] {
    lazy val tag = typeTag[BaseTableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(BaseTable)
  }
  implicit lazy val BaseTableCompanionElem: BaseTableCompanionElem = new BaseTableCompanionElem

  implicit def proxyBaseTable[R:Elem](p: Rep[BaseTable[R]]): BaseTable[R] =
    proxyOps[BaseTable[R]](p)

  implicit class ExtendedBaseTable[R](p: Rep[BaseTable[R]])(implicit schema: Elem[R]) {
    def toData: Rep[BaseTableData[R]] = isoBaseTable(schema).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoBaseTable[R](implicit schema: Elem[R]): Iso[BaseTableData[R], BaseTable[R]] =
    new BaseTableIso[R]

  // 6) smart constructor and deconstructor
  def mkBaseTable[R](tableName: Rep[String])(implicit schema: Elem[R]): Rep[BaseTable[R]]
  def unmkBaseTable[R:Elem](p: Rep[BaseTable[R]]): Option[(Rep[String])]

  // elem for concrete class
  class UniqueIndexElem[K, R](iso: Iso[UniqueIndexData[K, R], UniqueIndex[K, R]]) extends TableElem[R, UniqueIndexData[K, R], UniqueIndex[K, R]](iso)

  // state representation type
  type UniqueIndexData[K, R] = (String, (Table[R], (PMap[K,R], R => K)))

  // 3) Iso for concrete class
  class UniqueIndexIso[K, R](implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends Iso[UniqueIndexData[K, R], UniqueIndex[K, R]] {
    override def from(p: Rep[UniqueIndex[K, R]]) =
      unmkUniqueIndex(p) match {
        case Some((tableName, table, map, getKey)) => Pair(tableName, Pair(table, Pair(map, getKey)))
        case None => !!!
      }
    override def to(p: Rep[(String, (Table[R], (PMap[K,R], R => K)))]) = {
      val Pair(tableName, Pair(table, Pair(map, getKey))) = p
      UniqueIndex(tableName, table, map, getKey)
    }
    lazy val tag = {
      implicit val tagK = element[K].tag
      implicit val tagR = element[R].tag
      typeTag[UniqueIndex[K, R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[UniqueIndex[K, R]]](UniqueIndex("", element[Table[R]].defaultRepValue, element[PMap[K,R]].defaultRepValue, fun { (x: Rep[R]) => element[K].defaultRepValue }))
    lazy val eTo = new UniqueIndexElem[K, R](this)
  }
  // 4) constructor and deconstructor
  trait UniqueIndexCompanionAbs extends UniqueIndexCompanion {
    override def toString = "UniqueIndex"
    def apply[K, R](p: Rep[UniqueIndexData[K, R]])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[UniqueIndex[K, R]] =
      isoUniqueIndex(schema, index, keyPath).to(p)
    def apply[K, R](tableName: Rep[String], table: Rep[Table[R]], map: Rep[PMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[UniqueIndex[K, R]] =
      mkUniqueIndex(tableName, table, map, getKey)
    def unapply[K:Elem, R:Elem](p: Rep[UniqueIndex[K, R]]) = unmkUniqueIndex(p)
  }
  def UniqueIndex: Rep[UniqueIndexCompanionAbs]
  implicit def proxyUniqueIndexCompanion(p: Rep[UniqueIndexCompanionAbs]): UniqueIndexCompanionAbs = {
    proxyOps[UniqueIndexCompanionAbs](p)
  }

  class UniqueIndexCompanionElem extends CompanionElem[UniqueIndexCompanionAbs] {
    lazy val tag = typeTag[UniqueIndexCompanionAbs]
    lazy val defaultRep = Default.defaultVal(UniqueIndex)
  }
  implicit lazy val UniqueIndexCompanionElem: UniqueIndexCompanionElem = new UniqueIndexCompanionElem

  implicit def proxyUniqueIndex[K:Elem, R:Elem](p: Rep[UniqueIndex[K, R]]): UniqueIndex[K, R] =
    proxyOps[UniqueIndex[K, R]](p)

  implicit class ExtendedUniqueIndex[K, R](p: Rep[UniqueIndex[K, R]])(implicit schema: Elem[R], index: Elem[K], keyPath: String) {
    def toData: Rep[UniqueIndexData[K, R]] = isoUniqueIndex(schema, index, keyPath).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoUniqueIndex[K, R](implicit schema: Elem[R], index: Elem[K], keyPath: String): Iso[UniqueIndexData[K, R], UniqueIndex[K, R]] =
    new UniqueIndexIso[K, R]

  // 6) smart constructor and deconstructor
  def mkUniqueIndex[K, R](tableName: Rep[String], table: Rep[Table[R]], map: Rep[PMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[UniqueIndex[K, R]]
  def unmkUniqueIndex[K:Elem, R:Elem](p: Rep[UniqueIndex[K, R]]): Option[(Rep[String], Rep[Table[R]], Rep[PMap[K,R]], Rep[R => K])]

  // elem for concrete class
  class NonUniqueIndexElem[K, R](iso: Iso[NonUniqueIndexData[K, R], NonUniqueIndex[K, R]]) extends TableElem[R, NonUniqueIndexData[K, R], NonUniqueIndex[K, R]](iso)

  // state representation type
  type NonUniqueIndexData[K, R] = (String, (Table[R], (MultiMap[K,R], R => K)))

  // 3) Iso for concrete class
  class NonUniqueIndexIso[K, R](implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends Iso[NonUniqueIndexData[K, R], NonUniqueIndex[K, R]] {
    override def from(p: Rep[NonUniqueIndex[K, R]]) =
      unmkNonUniqueIndex(p) match {
        case Some((tableName, table, map, getKey)) => Pair(tableName, Pair(table, Pair(map, getKey)))
        case None => !!!
      }
    override def to(p: Rep[(String, (Table[R], (MultiMap[K,R], R => K)))]) = {
      val Pair(tableName, Pair(table, Pair(map, getKey))) = p
      NonUniqueIndex(tableName, table, map, getKey)
    }
    lazy val tag = {
      implicit val tagK = element[K].tag
      implicit val tagR = element[R].tag
      typeTag[NonUniqueIndex[K, R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[NonUniqueIndex[K, R]]](NonUniqueIndex("", element[Table[R]].defaultRepValue, element[MultiMap[K,R]].defaultRepValue, fun { (x: Rep[R]) => element[K].defaultRepValue }))
    lazy val eTo = new NonUniqueIndexElem[K, R](this)
  }
  // 4) constructor and deconstructor
  trait NonUniqueIndexCompanionAbs extends NonUniqueIndexCompanion {
    override def toString = "NonUniqueIndex"
    def apply[K, R](p: Rep[NonUniqueIndexData[K, R]])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[NonUniqueIndex[K, R]] =
      isoNonUniqueIndex(schema, index, keyPath).to(p)
    def apply[K, R](tableName: Rep[String], table: Rep[Table[R]], map: Rep[MultiMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[NonUniqueIndex[K, R]] =
      mkNonUniqueIndex(tableName, table, map, getKey)
    def unapply[K:Elem, R:Elem](p: Rep[NonUniqueIndex[K, R]]) = unmkNonUniqueIndex(p)
  }
  def NonUniqueIndex: Rep[NonUniqueIndexCompanionAbs]
  implicit def proxyNonUniqueIndexCompanion(p: Rep[NonUniqueIndexCompanionAbs]): NonUniqueIndexCompanionAbs = {
    proxyOps[NonUniqueIndexCompanionAbs](p)
  }

  class NonUniqueIndexCompanionElem extends CompanionElem[NonUniqueIndexCompanionAbs] {
    lazy val tag = typeTag[NonUniqueIndexCompanionAbs]
    lazy val defaultRep = Default.defaultVal(NonUniqueIndex)
  }
  implicit lazy val NonUniqueIndexCompanionElem: NonUniqueIndexCompanionElem = new NonUniqueIndexCompanionElem

  implicit def proxyNonUniqueIndex[K:Elem, R:Elem](p: Rep[NonUniqueIndex[K, R]]): NonUniqueIndex[K, R] =
    proxyOps[NonUniqueIndex[K, R]](p)

  implicit class ExtendedNonUniqueIndex[K, R](p: Rep[NonUniqueIndex[K, R]])(implicit schema: Elem[R], index: Elem[K], keyPath: String) {
    def toData: Rep[NonUniqueIndexData[K, R]] = isoNonUniqueIndex(schema, index, keyPath).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoNonUniqueIndex[K, R](implicit schema: Elem[R], index: Elem[K], keyPath: String): Iso[NonUniqueIndexData[K, R], NonUniqueIndex[K, R]] =
    new NonUniqueIndexIso[K, R]

  // 6) smart constructor and deconstructor
  def mkNonUniqueIndex[K, R](tableName: Rep[String], table: Rep[Table[R]], map: Rep[MultiMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String): Rep[NonUniqueIndex[K, R]]
  def unmkNonUniqueIndex[K:Elem, R:Elem](p: Rep[NonUniqueIndex[K, R]]): Option[(Rep[String], Rep[Table[R]], Rep[MultiMap[K,R]], Rep[R => K])]

  // elem for concrete class
  class DatabaseTableElem[R](iso: Iso[DatabaseTableData[R], DatabaseTable[R]]) extends TableElem[R, DatabaseTableData[R], DatabaseTable[R]](iso)

  // state representation type
  type DatabaseTableData[R] = (String, ArrayBuffer[R])

  // 3) Iso for concrete class
  class DatabaseTableIso[R](implicit schema: Elem[R])
    extends Iso[DatabaseTableData[R], DatabaseTable[R]] {
    override def from(p: Rep[DatabaseTable[R]]) =
      unmkDatabaseTable(p) match {
        case Some((tableName, records)) => Pair(tableName, records)
        case None => !!!
      }
    override def to(p: Rep[(String, ArrayBuffer[R])]) = {
      val Pair(tableName, records) = p
      DatabaseTable(tableName, records)
    }
    lazy val tag = {
      implicit val tagR = element[R].tag
      typeTag[DatabaseTable[R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[DatabaseTable[R]]](DatabaseTable("", element[ArrayBuffer[R]].defaultRepValue))
    lazy val eTo = new DatabaseTableElem[R](this)
  }
  // 4) constructor and deconstructor
  trait DatabaseTableCompanionAbs extends DatabaseTableCompanion {
    override def toString = "DatabaseTable"
    def apply[R](p: Rep[DatabaseTableData[R]])(implicit schema: Elem[R]): Rep[DatabaseTable[R]] =
      isoDatabaseTable(schema).to(p)
    def apply[R](tableName: Rep[String], records: Rep[ArrayBuffer[R]])(implicit schema: Elem[R]): Rep[DatabaseTable[R]] =
      mkDatabaseTable(tableName, records)
    def unapply[R:Elem](p: Rep[DatabaseTable[R]]) = unmkDatabaseTable(p)
  }
  def DatabaseTable: Rep[DatabaseTableCompanionAbs]
  implicit def proxyDatabaseTableCompanion(p: Rep[DatabaseTableCompanionAbs]): DatabaseTableCompanionAbs = {
    proxyOps[DatabaseTableCompanionAbs](p)
  }

  class DatabaseTableCompanionElem extends CompanionElem[DatabaseTableCompanionAbs] {
    lazy val tag = typeTag[DatabaseTableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(DatabaseTable)
  }
  implicit lazy val DatabaseTableCompanionElem: DatabaseTableCompanionElem = new DatabaseTableCompanionElem

  implicit def proxyDatabaseTable[R:Elem](p: Rep[DatabaseTable[R]]): DatabaseTable[R] =
    proxyOps[DatabaseTable[R]](p)

  implicit class ExtendedDatabaseTable[R](p: Rep[DatabaseTable[R]])(implicit schema: Elem[R]) {
    def toData: Rep[DatabaseTableData[R]] = isoDatabaseTable(schema).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoDatabaseTable[R](implicit schema: Elem[R]): Iso[DatabaseTableData[R], DatabaseTable[R]] =
    new DatabaseTableIso[R]

  // 6) smart constructor and deconstructor
  def mkDatabaseTable[R](tableName: Rep[String], records: Rep[ArrayBuffer[R]])(implicit schema: Elem[R]): Rep[DatabaseTable[R]]
  def unmkDatabaseTable[R:Elem](p: Rep[DatabaseTable[R]]): Option[(Rep[String], Rep[ArrayBuffer[R]])]

  // elem for concrete class
  class TemporaryTableElem[R](iso: Iso[TemporaryTableData[R], TemporaryTable[R]]) extends TableElem[R, TemporaryTableData[R], TemporaryTable[R]](iso)

  // state representation type
  type TemporaryTableData[R] = Array[R]

  // 3) Iso for concrete class
  class TemporaryTableIso[R](implicit schema: Elem[R])
    extends Iso[TemporaryTableData[R], TemporaryTable[R]] {
    override def from(p: Rep[TemporaryTable[R]]) =
      unmkTemporaryTable(p) match {
        case Some((records)) => records
        case None => !!!
      }
    override def to(p: Rep[Array[R]]) = {
      val records = p
      TemporaryTable(records)
    }
    lazy val tag = {
      implicit val tagR = element[R].tag
      typeTag[TemporaryTable[R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[TemporaryTable[R]]](TemporaryTable(element[Array[R]].defaultRepValue))
    lazy val eTo = new TemporaryTableElem[R](this)
  }
  // 4) constructor and deconstructor
  trait TemporaryTableCompanionAbs extends TemporaryTableCompanion {
    override def toString = "TemporaryTable"

    def apply[R](records: Rep[Array[R]])(implicit schema: Elem[R]): Rep[TemporaryTable[R]] =
      mkTemporaryTable(records)
    def unapply[R:Elem](p: Rep[TemporaryTable[R]]) = unmkTemporaryTable(p)
  }
  def TemporaryTable: Rep[TemporaryTableCompanionAbs]
  implicit def proxyTemporaryTableCompanion(p: Rep[TemporaryTableCompanionAbs]): TemporaryTableCompanionAbs = {
    proxyOps[TemporaryTableCompanionAbs](p)
  }

  class TemporaryTableCompanionElem extends CompanionElem[TemporaryTableCompanionAbs] {
    lazy val tag = typeTag[TemporaryTableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(TemporaryTable)
  }
  implicit lazy val TemporaryTableCompanionElem: TemporaryTableCompanionElem = new TemporaryTableCompanionElem

  implicit def proxyTemporaryTable[R:Elem](p: Rep[TemporaryTable[R]]): TemporaryTable[R] =
    proxyOps[TemporaryTable[R]](p)

  implicit class ExtendedTemporaryTable[R](p: Rep[TemporaryTable[R]])(implicit schema: Elem[R]) {
    def toData: Rep[TemporaryTableData[R]] = isoTemporaryTable(schema).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoTemporaryTable[R](implicit schema: Elem[R]): Iso[TemporaryTableData[R], TemporaryTable[R]] =
    new TemporaryTableIso[R]

  // 6) smart constructor and deconstructor
  def mkTemporaryTable[R](records: Rep[Array[R]])(implicit schema: Elem[R]): Rep[TemporaryTable[R]]
  def unmkTemporaryTable[R:Elem](p: Rep[TemporaryTable[R]]): Option[(Rep[Array[R]])]

  // elem for concrete class
  class PairTableElem[R1, R2](iso: Iso[PairTableData[R1, R2], PairTable[R1, R2]]) extends TableElem[(R1,R2), PairTableData[R1, R2], PairTable[R1, R2]](iso)

  // state representation type
  type PairTableData[R1, R2] = (Table[R1], Table[R2])

  // 3) Iso for concrete class
  class PairTableIso[R1, R2](implicit leftSchema: Elem[R1], rightSchema: Elem[R2])
    extends Iso[PairTableData[R1, R2], PairTable[R1, R2]] {
    override def from(p: Rep[PairTable[R1, R2]]) =
      unmkPairTable(p) match {
        case Some((left, right)) => Pair(left, right)
        case None => !!!
      }
    override def to(p: Rep[(Table[R1], Table[R2])]) = {
      val Pair(left, right) = p
      PairTable(left, right)
    }
    lazy val tag = {
      implicit val tagR1 = element[R1].tag
      implicit val tagR2 = element[R2].tag
      typeTag[PairTable[R1, R2]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[PairTable[R1, R2]]](PairTable(element[Table[R1]].defaultRepValue, element[Table[R2]].defaultRepValue))
    lazy val eTo = new PairTableElem[R1, R2](this)
  }
  // 4) constructor and deconstructor
  trait PairTableCompanionAbs extends PairTableCompanion {
    override def toString = "PairTable"
    def apply[R1, R2](p: Rep[PairTableData[R1, R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]): Rep[PairTable[R1, R2]] =
      isoPairTable(leftSchema, rightSchema).to(p)
    def apply[R1, R2](left: Rep[Table[R1]], right: Rep[Table[R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]): Rep[PairTable[R1, R2]] =
      mkPairTable(left, right)
    def unapply[R1:Elem, R2:Elem](p: Rep[PairTable[R1, R2]]) = unmkPairTable(p)
  }
  def PairTable: Rep[PairTableCompanionAbs]
  implicit def proxyPairTableCompanion(p: Rep[PairTableCompanionAbs]): PairTableCompanionAbs = {
    proxyOps[PairTableCompanionAbs](p)
  }

  class PairTableCompanionElem extends CompanionElem[PairTableCompanionAbs] {
    lazy val tag = typeTag[PairTableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(PairTable)
  }
  implicit lazy val PairTableCompanionElem: PairTableCompanionElem = new PairTableCompanionElem

  implicit def proxyPairTable[R1:Elem, R2:Elem](p: Rep[PairTable[R1, R2]]): PairTable[R1, R2] =
    proxyOps[PairTable[R1, R2]](p)

  implicit class ExtendedPairTable[R1, R2](p: Rep[PairTable[R1, R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]) {
    def toData: Rep[PairTableData[R1, R2]] = isoPairTable(leftSchema, rightSchema).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoPairTable[R1, R2](implicit leftSchema: Elem[R1], rightSchema: Elem[R2]): Iso[PairTableData[R1, R2], PairTable[R1, R2]] =
    new PairTableIso[R1, R2]

  // 6) smart constructor and deconstructor
  def mkPairTable[R1, R2](left: Rep[Table[R1]], right: Rep[Table[R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]): Rep[PairTable[R1, R2]]
  def unmkPairTable[R1:Elem, R2:Elem](p: Rep[PairTable[R1, R2]]): Option[(Rep[Table[R1]], Rep[Table[R2]])]

  // elem for concrete class
  class ShardedTableElem[R](iso: Iso[ShardedTableData[R], ShardedTable[R]]) extends TableElem[R, ShardedTableData[R], ShardedTable[R]](iso)

  // state representation type
  type ShardedTableData[R] = (String, (Int, (R => Int, Array[Table[R]])))

  // 3) Iso for concrete class
  class ShardedTableIso[R](implicit schema: Elem[R], shardKeyPath: String)
    extends Iso[ShardedTableData[R], ShardedTable[R]] {
    override def from(p: Rep[ShardedTable[R]]) =
      unmkShardedTable(p) match {
        case Some((tableName, nShards, distrib, shards)) => Pair(tableName, Pair(nShards, Pair(distrib, shards)))
        case None => !!!
      }
    override def to(p: Rep[(String, (Int, (R => Int, Array[Table[R]])))]) = {
      val Pair(tableName, Pair(nShards, Pair(distrib, shards))) = p
      ShardedTable(tableName, nShards, distrib, shards)
    }
    lazy val tag = {
      implicit val tagR = element[R].tag
      typeTag[ShardedTable[R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[ShardedTable[R]]](ShardedTable("", 0, fun { (x: Rep[R]) => 0 }, element[Array[Table[R]]].defaultRepValue))
    lazy val eTo = new ShardedTableElem[R](this)
  }
  // 4) constructor and deconstructor
  trait ShardedTableCompanionAbs extends ShardedTableCompanion {
    override def toString = "ShardedTable"
    def apply[R](p: Rep[ShardedTableData[R]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedTable[R]] =
      isoShardedTable(schema, shardKeyPath).to(p)
    def apply[R](tableName: Rep[String], nShards: Rep[Int], distrib: Rep[R => Int], shards: Rep[Array[Table[R]]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedTable[R]] =
      mkShardedTable(tableName, nShards, distrib, shards)
    def unapply[R:Elem](p: Rep[ShardedTable[R]]) = unmkShardedTable(p)
  }
  def ShardedTable: Rep[ShardedTableCompanionAbs]
  implicit def proxyShardedTableCompanion(p: Rep[ShardedTableCompanionAbs]): ShardedTableCompanionAbs = {
    proxyOps[ShardedTableCompanionAbs](p)
  }

  class ShardedTableCompanionElem extends CompanionElem[ShardedTableCompanionAbs] {
    lazy val tag = typeTag[ShardedTableCompanionAbs]
    lazy val defaultRep = Default.defaultVal(ShardedTable)
  }
  implicit lazy val ShardedTableCompanionElem: ShardedTableCompanionElem = new ShardedTableCompanionElem

  implicit def proxyShardedTable[R:Elem](p: Rep[ShardedTable[R]]): ShardedTable[R] =
    proxyOps[ShardedTable[R]](p)

  implicit class ExtendedShardedTable[R](p: Rep[ShardedTable[R]])(implicit schema: Elem[R], shardKeyPath: String) {
    def toData: Rep[ShardedTableData[R]] = isoShardedTable(schema, shardKeyPath).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoShardedTable[R](implicit schema: Elem[R], shardKeyPath: String): Iso[ShardedTableData[R], ShardedTable[R]] =
    new ShardedTableIso[R]

  // 6) smart constructor and deconstructor
  def mkShardedTable[R](tableName: Rep[String], nShards: Rep[Int], distrib: Rep[R => Int], shards: Rep[Array[Table[R]]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedTable[R]]
  def unmkShardedTable[R:Elem](p: Rep[ShardedTable[R]]): Option[(Rep[String], Rep[Int], Rep[R => Int], Rep[Array[Table[R]]])]

  // elem for concrete class
  class ShardedViewElem[R](iso: Iso[ShardedViewData[R], ShardedView[R]]) extends TableElem[R, ShardedViewData[R], ShardedView[R]](iso)

  // state representation type
  type ShardedViewData[R] = (Int, Int => Table[R])

  // 3) Iso for concrete class
  class ShardedViewIso[R](implicit schema: Elem[R], shardKeyPath: String)
    extends Iso[ShardedViewData[R], ShardedView[R]] {
    override def from(p: Rep[ShardedView[R]]) =
      unmkShardedView(p) match {
        case Some((nShards, view)) => Pair(nShards, view)
        case None => !!!
      }
    override def to(p: Rep[(Int, Int => Table[R])]) = {
      val Pair(nShards, view) = p
      ShardedView(nShards, view)
    }
    lazy val tag = {
      implicit val tagR = element[R].tag
      typeTag[ShardedView[R]]
    }
    lazy val defaultRepTo = Default.defaultVal[Rep[ShardedView[R]]](ShardedView(0, fun { (x: Rep[Int]) => element[Table[R]].defaultRepValue }))
    lazy val eTo = new ShardedViewElem[R](this)
  }
  // 4) constructor and deconstructor
  trait ShardedViewCompanionAbs extends ShardedViewCompanion {
    override def toString = "ShardedView"
    def apply[R](p: Rep[ShardedViewData[R]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedView[R]] =
      isoShardedView(schema, shardKeyPath).to(p)
    def apply[R](nShards: Rep[Int], view: Rep[Int => Table[R]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedView[R]] =
      mkShardedView(nShards, view)
    def unapply[R:Elem](p: Rep[ShardedView[R]]) = unmkShardedView(p)
  }
  def ShardedView: Rep[ShardedViewCompanionAbs]
  implicit def proxyShardedViewCompanion(p: Rep[ShardedViewCompanionAbs]): ShardedViewCompanionAbs = {
    proxyOps[ShardedViewCompanionAbs](p)
  }

  class ShardedViewCompanionElem extends CompanionElem[ShardedViewCompanionAbs] {
    lazy val tag = typeTag[ShardedViewCompanionAbs]
    lazy val defaultRep = Default.defaultVal(ShardedView)
  }
  implicit lazy val ShardedViewCompanionElem: ShardedViewCompanionElem = new ShardedViewCompanionElem

  implicit def proxyShardedView[R:Elem](p: Rep[ShardedView[R]]): ShardedView[R] =
    proxyOps[ShardedView[R]](p)

  implicit class ExtendedShardedView[R](p: Rep[ShardedView[R]])(implicit schema: Elem[R], shardKeyPath: String) {
    def toData: Rep[ShardedViewData[R]] = isoShardedView(schema, shardKeyPath).from(p)
  }

  // 5) implicit resolution of Iso
  implicit def isoShardedView[R](implicit schema: Elem[R], shardKeyPath: String): Iso[ShardedViewData[R], ShardedView[R]] =
    new ShardedViewIso[R]

  // 6) smart constructor and deconstructor
  def mkShardedView[R](nShards: Rep[Int], view: Rep[Int => Table[R]])(implicit schema: Elem[R], shardKeyPath: String): Rep[ShardedView[R]]
  def unmkShardedView[R:Elem](p: Rep[ShardedView[R]]): Option[(Rep[Int], Rep[Int => Table[R]])]
}

trait SqlSeq extends SqlAbs { self: ScalanSeq with SqlDsl =>
  lazy val Table: Rep[TableCompanionAbs] = new TableCompanionAbs with UserTypeSeq[TableCompanionAbs, TableCompanionAbs] {
    lazy val selfType = element[TableCompanionAbs]
  }

  case class SeqBaseTable[R]
      (override val tableName: Rep[String])
      (implicit schema: Elem[R])
    extends BaseTable[R](tableName) with UserTypeSeq[Table[R], BaseTable[R]] {
    lazy val selfType = element[BaseTable[R]].asInstanceOf[Elem[Table[R]]]
  }
  lazy val BaseTable = new BaseTableCompanionAbs with UserTypeSeq[BaseTableCompanionAbs, BaseTableCompanionAbs] {
    lazy val selfType = element[BaseTableCompanionAbs]
  }

  def mkBaseTable[R]
      (tableName: Rep[String])(implicit schema: Elem[R]) =
      new SeqBaseTable[R](tableName)
  def unmkBaseTable[R:Elem](p: Rep[BaseTable[R]]) =
    Some((p.tableName))

  case class SeqUniqueIndex[K, R]
      (override val tableName: Rep[String], override val table: Rep[Table[R]], override val map: Rep[PMap[K,R]], override val getKey: Rep[R => K])
      (implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends UniqueIndex[K, R](tableName, table, map, getKey) with UserTypeSeq[BaseTable[R], UniqueIndex[K, R]] {
    lazy val selfType = element[UniqueIndex[K, R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val UniqueIndex = new UniqueIndexCompanionAbs with UserTypeSeq[UniqueIndexCompanionAbs, UniqueIndexCompanionAbs] {
    lazy val selfType = element[UniqueIndexCompanionAbs]
  }

  def mkUniqueIndex[K, R]
      (tableName: Rep[String], table: Rep[Table[R]], map: Rep[PMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String) =
      new SeqUniqueIndex[K, R](tableName, table, map, getKey)
  def unmkUniqueIndex[K:Elem, R:Elem](p: Rep[UniqueIndex[K, R]]) =
    Some((p.tableName, p.table, p.map, p.getKey))

  case class SeqNonUniqueIndex[K, R]
      (override val tableName: Rep[String], override val table: Rep[Table[R]], override val map: Rep[MultiMap[K,R]], override val getKey: Rep[R => K])
      (implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends NonUniqueIndex[K, R](tableName, table, map, getKey) with UserTypeSeq[BaseTable[R], NonUniqueIndex[K, R]] {
    lazy val selfType = element[NonUniqueIndex[K, R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val NonUniqueIndex = new NonUniqueIndexCompanionAbs with UserTypeSeq[NonUniqueIndexCompanionAbs, NonUniqueIndexCompanionAbs] {
    lazy val selfType = element[NonUniqueIndexCompanionAbs]
  }

  def mkNonUniqueIndex[K, R]
      (tableName: Rep[String], table: Rep[Table[R]], map: Rep[MultiMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String) =
      new SeqNonUniqueIndex[K, R](tableName, table, map, getKey)
  def unmkNonUniqueIndex[K:Elem, R:Elem](p: Rep[NonUniqueIndex[K, R]]) =
    Some((p.tableName, p.table, p.map, p.getKey))

  case class SeqDatabaseTable[R]
      (override val tableName: Rep[String], override val records: Rep[ArrayBuffer[R]])
      (implicit schema: Elem[R])
    extends DatabaseTable[R](tableName, records) with UserTypeSeq[BaseTable[R], DatabaseTable[R]] {
    lazy val selfType = element[DatabaseTable[R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val DatabaseTable = new DatabaseTableCompanionAbs with UserTypeSeq[DatabaseTableCompanionAbs, DatabaseTableCompanionAbs] {
    lazy val selfType = element[DatabaseTableCompanionAbs]
  }

  def mkDatabaseTable[R]
      (tableName: Rep[String], records: Rep[ArrayBuffer[R]])(implicit schema: Elem[R]) =
      new SeqDatabaseTable[R](tableName, records)
  def unmkDatabaseTable[R:Elem](p: Rep[DatabaseTable[R]]) =
    Some((p.tableName, p.records))

  case class SeqTemporaryTable[R]
      (override val records: Rep[Array[R]])
      (implicit schema: Elem[R])
    extends TemporaryTable[R](records) with UserTypeSeq[BaseTable[R], TemporaryTable[R]] {
    lazy val selfType = element[TemporaryTable[R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val TemporaryTable = new TemporaryTableCompanionAbs with UserTypeSeq[TemporaryTableCompanionAbs, TemporaryTableCompanionAbs] {
    lazy val selfType = element[TemporaryTableCompanionAbs]
  }

  def mkTemporaryTable[R]
      (records: Rep[Array[R]])(implicit schema: Elem[R]) =
      new SeqTemporaryTable[R](records)
  def unmkTemporaryTable[R:Elem](p: Rep[TemporaryTable[R]]) =
    Some((p.records))

  case class SeqPairTable[R1, R2]
      (override val left: Rep[Table[R1]], override val right: Rep[Table[R2]])
      (implicit leftSchema: Elem[R1], rightSchema: Elem[R2])
    extends PairTable[R1, R2](left, right) with UserTypeSeq[BaseTable[(R1,R2)], PairTable[R1, R2]] {
    lazy val selfType = element[PairTable[R1, R2]].asInstanceOf[Elem[BaseTable[(R1,R2)]]]
  }
  lazy val PairTable = new PairTableCompanionAbs with UserTypeSeq[PairTableCompanionAbs, PairTableCompanionAbs] {
    lazy val selfType = element[PairTableCompanionAbs]
  }

  def mkPairTable[R1, R2]
      (left: Rep[Table[R1]], right: Rep[Table[R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]) =
      new SeqPairTable[R1, R2](left, right)
  def unmkPairTable[R1:Elem, R2:Elem](p: Rep[PairTable[R1, R2]]) =
    Some((p.left, p.right))

  case class SeqShardedTable[R]
      (override val tableName: Rep[String], override val nShards: Rep[Int], override val distrib: Rep[R => Int], override val shards: Rep[Array[Table[R]]])
      (implicit schema: Elem[R], shardKeyPath: String)
    extends ShardedTable[R](tableName, nShards, distrib, shards) with UserTypeSeq[BaseTable[R], ShardedTable[R]] {
    lazy val selfType = element[ShardedTable[R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val ShardedTable = new ShardedTableCompanionAbs with UserTypeSeq[ShardedTableCompanionAbs, ShardedTableCompanionAbs] {
    lazy val selfType = element[ShardedTableCompanionAbs]
  }

  def mkShardedTable[R]
      (tableName: Rep[String], nShards: Rep[Int], distrib: Rep[R => Int], shards: Rep[Array[Table[R]]])(implicit schema: Elem[R], shardKeyPath: String) =
      new SeqShardedTable[R](tableName, nShards, distrib, shards)
  def unmkShardedTable[R:Elem](p: Rep[ShardedTable[R]]) =
    Some((p.tableName, p.nShards, p.distrib, p.shards))

  case class SeqShardedView[R]
      (override val nShards: Rep[Int], override val view: Rep[Int => Table[R]])
      (implicit schema: Elem[R], shardKeyPath: String)
    extends ShardedView[R](nShards, view) with UserTypeSeq[BaseTable[R], ShardedView[R]] {
    lazy val selfType = element[ShardedView[R]].asInstanceOf[Elem[BaseTable[R]]]
  }
  lazy val ShardedView = new ShardedViewCompanionAbs with UserTypeSeq[ShardedViewCompanionAbs, ShardedViewCompanionAbs] {
    lazy val selfType = element[ShardedViewCompanionAbs]
  }

  def mkShardedView[R]
      (nShards: Rep[Int], view: Rep[Int => Table[R]])(implicit schema: Elem[R], shardKeyPath: String) =
      new SeqShardedView[R](nShards, view)
  def unmkShardedView[R:Elem](p: Rep[ShardedView[R]]) =
    Some((p.nShards, p.view))
}

trait SqlExp extends SqlAbs { self: ScalanExp with SqlDsl =>
  lazy val Table: Rep[TableCompanionAbs] = new TableCompanionAbs with UserTypeDef[TableCompanionAbs, TableCompanionAbs] {
    lazy val selfType = element[TableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  case class ExpBaseTable[R]
      (override val tableName: Rep[String])
      (implicit schema: Elem[R])
    extends BaseTable[R](tableName) with UserTypeDef[Table[R], BaseTable[R]] {
    lazy val selfType = element[BaseTable[R]].asInstanceOf[Elem[Table[R]]]
    override def mirror(t: Transformer) = ExpBaseTable[R](t(tableName))
  }

  lazy val BaseTable: Rep[BaseTableCompanionAbs] = new BaseTableCompanionAbs with UserTypeDef[BaseTableCompanionAbs, BaseTableCompanionAbs] {
    lazy val selfType = element[BaseTableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object BaseTableMethods {
    object equals {
      def unapply(d: Def[_]): Option[(Rep[BaseTable[R]], Any) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(other, _*)) if method.getName == "equals" && receiver.elem.isInstanceOf[BaseTableElem[_]] =>
          Some((receiver, other)).asInstanceOf[Option[(Rep[BaseTable[R]], Any) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[BaseTable[R]], Any) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object PrimaryKey {
      def unapply(d: Def[_]): Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "PrimaryKey" && receiver.elem.isInstanceOf[BaseTableElem[_]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object SecondaryKey {
      def unapply(d: Def[_]): Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "SecondaryKey" && receiver.elem.isInstanceOf[BaseTableElem[_]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[BaseTable[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[BaseTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[BaseTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[BaseTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[BaseTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[BaseTable[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[BaseTableElem[_]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[BaseTable[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[BaseTable[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object BaseTableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[BaseTableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkBaseTable[R]
    (tableName: Rep[String])(implicit schema: Elem[R]) =
    new ExpBaseTable[R](tableName)
  def unmkBaseTable[R:Elem](p: Rep[BaseTable[R]]) =
    Some((p.tableName))

  case class ExpUniqueIndex[K, R]
      (override val tableName: Rep[String], override val table: Rep[Table[R]], override val map: Rep[PMap[K,R]], override val getKey: Rep[R => K])
      (implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends UniqueIndex[K, R](tableName, table, map, getKey) with UserTypeDef[BaseTable[R], UniqueIndex[K, R]] {
    lazy val selfType = element[UniqueIndex[K, R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpUniqueIndex[K, R](t(tableName), t(table), t(map), t(getKey))
  }

  lazy val UniqueIndex: Rep[UniqueIndexCompanionAbs] = new UniqueIndexCompanionAbs with UserTypeDef[UniqueIndexCompanionAbs, UniqueIndexCompanionAbs] {
    lazy val selfType = element[UniqueIndexCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object UniqueIndexMethods {
    object filter {
      def unapply(d: Def[_]): Option[(Rep[UniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "filter" && receiver.elem.isInstanceOf[UniqueIndexElem[_, _]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[UniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[UniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object count {
      def unapply(d: Def[_]): Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[UniqueIndexElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[UniqueIndex[K, R]], Rep[R]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[UniqueIndexElem[_, _]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[UniqueIndex[K, R]], Rep[R]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[UniqueIndex[K, R]], Rep[R]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Join {
      def unapply(d: Def[_]): Option[(Rep[UniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}] = d match {
        case MethodCall(receiver, method, Seq(inner, outKey, inKey, _*)) if method.getName == "Join" && receiver.elem.isInstanceOf[UniqueIndexElem[_, _]] =>
          Some((receiver, inner, outKey, inKey)).asInstanceOf[Option[(Rep[UniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[UniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[UniqueIndexElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[UniqueIndex[K, R]] forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object UniqueIndexCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[UniqueIndexCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(tableName, table, key, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[UniqueIndexCompanionElem] =>
          Some((tableName, table, key)).asInstanceOf[Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkUniqueIndex[K, R]
    (tableName: Rep[String], table: Rep[Table[R]], map: Rep[PMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String) =
    new ExpUniqueIndex[K, R](tableName, table, map, getKey)
  def unmkUniqueIndex[K:Elem, R:Elem](p: Rep[UniqueIndex[K, R]]) =
    Some((p.tableName, p.table, p.map, p.getKey))

  case class ExpNonUniqueIndex[K, R]
      (override val tableName: Rep[String], override val table: Rep[Table[R]], override val map: Rep[MultiMap[K,R]], override val getKey: Rep[R => K])
      (implicit schema: Elem[R], index: Elem[K], keyPath: String)
    extends NonUniqueIndex[K, R](tableName, table, map, getKey) with UserTypeDef[BaseTable[R], NonUniqueIndex[K, R]] {
    lazy val selfType = element[NonUniqueIndex[K, R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpNonUniqueIndex[K, R](t(tableName), t(table), t(map), t(getKey))
  }

  lazy val NonUniqueIndex: Rep[NonUniqueIndexCompanionAbs] = new NonUniqueIndexCompanionAbs with UserTypeDef[NonUniqueIndexCompanionAbs, NonUniqueIndexCompanionAbs] {
    lazy val selfType = element[NonUniqueIndexCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object NonUniqueIndexMethods {
    object filter {
      def unapply(d: Def[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "filter" && receiver.elem.isInstanceOf[NonUniqueIndexElem[_, _]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[NonUniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[R => Boolean]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object count {
      def unapply(d: Def[_]): Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[NonUniqueIndexElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[R]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[NonUniqueIndexElem[_, _]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[NonUniqueIndex[K, R]], Rep[R]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[R]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Join {
      def unapply(d: Def[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}] = d match {
        case MethodCall(receiver, method, Seq(inner, outKey, inKey, _*)) if method.getName == "Join" && receiver.elem.isInstanceOf[NonUniqueIndexElem[_, _]] =>
          Some((receiver, inner, outKey, inKey)).asInstanceOf[Option[(Rep[NonUniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[NonUniqueIndex[K, R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type K; type R; type I}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[NonUniqueIndexElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[NonUniqueIndex[K, R]] forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object NonUniqueIndexCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[NonUniqueIndexCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}] = d match {
        case MethodCall(receiver, method, Seq(tableName, table, key, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[NonUniqueIndexCompanionElem] =>
          Some((tableName, table, key)).asInstanceOf[Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[String], Rep[Table[R]], Rep[R => K]) forSome {type K; type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkNonUniqueIndex[K, R]
    (tableName: Rep[String], table: Rep[Table[R]], map: Rep[MultiMap[K,R]], getKey: Rep[R => K])(implicit schema: Elem[R], index: Elem[K], keyPath: String) =
    new ExpNonUniqueIndex[K, R](tableName, table, map, getKey)
  def unmkNonUniqueIndex[K:Elem, R:Elem](p: Rep[NonUniqueIndex[K, R]]) =
    Some((p.tableName, p.table, p.map, p.getKey))

  case class ExpDatabaseTable[R]
      (override val tableName: Rep[String], override val records: Rep[ArrayBuffer[R]])
      (implicit schema: Elem[R])
    extends DatabaseTable[R](tableName, records) with UserTypeDef[BaseTable[R], DatabaseTable[R]] {
    lazy val selfType = element[DatabaseTable[R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpDatabaseTable[R](t(tableName), t(records))
  }

  lazy val DatabaseTable: Rep[DatabaseTableCompanionAbs] = new DatabaseTableCompanionAbs with UserTypeDef[DatabaseTableCompanionAbs, DatabaseTableCompanionAbs] {
    lazy val selfType = element[DatabaseTableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object DatabaseTableMethods {
    object count {
      def unapply(d: Def[_]): Option[Rep[DatabaseTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[DatabaseTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[DatabaseTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[DatabaseTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[DatabaseTable[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[DatabaseTableElem[_]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[DatabaseTable[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[DatabaseTable[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insertFrom {
      def unapply(d: Def[_]): Option[(Rep[DatabaseTable[R]], Arr[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(arr, _*)) if method.getName == "insertFrom" && receiver.elem.isInstanceOf[DatabaseTableElem[_]] =>
          Some((receiver, arr)).asInstanceOf[Option[(Rep[DatabaseTable[R]], Arr[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[DatabaseTable[R]], Arr[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[DatabaseTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[DatabaseTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[DatabaseTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[DatabaseTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object DatabaseTableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[DatabaseTableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[Rep[String] forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(tableName, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[DatabaseTableCompanionElem] =>
          Some(tableName).asInstanceOf[Option[Rep[String] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[String] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object createShard {
      def unapply(d: Def[_]): Option[Rep[Int] forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(shardNo, _*)) if method.getName == "createShard" && receiver.elem.isInstanceOf[DatabaseTableCompanionElem] =>
          Some(shardNo).asInstanceOf[Option[Rep[Int] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Int] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkDatabaseTable[R]
    (tableName: Rep[String], records: Rep[ArrayBuffer[R]])(implicit schema: Elem[R]) =
    new ExpDatabaseTable[R](tableName, records)
  def unmkDatabaseTable[R:Elem](p: Rep[DatabaseTable[R]]) =
    Some((p.tableName, p.records))

  case class ExpTemporaryTable[R]
      (override val records: Rep[Array[R]])
      (implicit schema: Elem[R])
    extends TemporaryTable[R](records) with UserTypeDef[BaseTable[R], TemporaryTable[R]] {
    lazy val selfType = element[TemporaryTable[R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpTemporaryTable[R](t(records))
  }

  lazy val TemporaryTable: Rep[TemporaryTableCompanionAbs] = new TemporaryTableCompanionAbs with UserTypeDef[TemporaryTableCompanionAbs, TemporaryTableCompanionAbs] {
    lazy val selfType = element[TemporaryTableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object TemporaryTableMethods {
    object insert {
      def unapply(d: Def[_]): Option[(Rep[TemporaryTable[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[TemporaryTableElem[_]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[TemporaryTable[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[TemporaryTable[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object PrimaryKey {
      def unapply(d: Def[_]): Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "PrimaryKey" && receiver.elem.isInstanceOf[TemporaryTableElem[_]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object SecondaryKey {
      def unapply(d: Def[_]): Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "SecondaryKey" && receiver.elem.isInstanceOf[TemporaryTableElem[_]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[TemporaryTable[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[TemporaryTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[TemporaryTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[TemporaryTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[TemporaryTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object TemporaryTableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[TemporaryTableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object from {
      def unapply(d: Def[_]): Option[Arr[R] forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(data, _*)) if method.getName == "from" && receiver.elem.isInstanceOf[TemporaryTableCompanionElem] =>
          Some(data).asInstanceOf[Option[Arr[R] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Arr[R] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkTemporaryTable[R]
    (records: Rep[Array[R]])(implicit schema: Elem[R]) =
    new ExpTemporaryTable[R](records)
  def unmkTemporaryTable[R:Elem](p: Rep[TemporaryTable[R]]) =
    Some((p.records))

  case class ExpPairTable[R1, R2]
      (override val left: Rep[Table[R1]], override val right: Rep[Table[R2]])
      (implicit leftSchema: Elem[R1], rightSchema: Elem[R2])
    extends PairTable[R1, R2](left, right) with UserTypeDef[BaseTable[(R1,R2)], PairTable[R1, R2]] {
    lazy val selfType = element[PairTable[R1, R2]].asInstanceOf[Elem[BaseTable[(R1,R2)]]]
    override def mirror(t: Transformer) = ExpPairTable[R1, R2](t(left), t(right))
  }

  lazy val PairTable: Rep[PairTableCompanionAbs] = new PairTableCompanionAbs with UserTypeDef[PairTableCompanionAbs, PairTableCompanionAbs] {
    lazy val selfType = element[PairTableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object PairTableMethods {
    object count {
      def unapply(d: Def[_]): Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[PairTableElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[PairTable[R1, R2]], Rep[(R1,R2)]) forSome {type R1; type R2}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[PairTableElem[_, _]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[PairTable[R1, R2]], Rep[(R1,R2)]) forSome {type R1; type R2}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[PairTable[R1, R2]], Rep[(R1,R2)]) forSome {type R1; type R2}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[PairTableElem[_, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[PairTable[R1, R2]] forSome {type R1; type R2}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object PairTableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R1; type R2}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[PairTableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R1; type R2}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R1; type R2}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[(Rep[Table[R1]], Rep[Table[R2]]) forSome {type R1; type R2}] = d match {
        case MethodCall(receiver, method, Seq(l, r, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[PairTableCompanionElem] =>
          Some((l, r)).asInstanceOf[Option[(Rep[Table[R1]], Rep[Table[R2]]) forSome {type R1; type R2}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R1]], Rep[Table[R2]]) forSome {type R1; type R2}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkPairTable[R1, R2]
    (left: Rep[Table[R1]], right: Rep[Table[R2]])(implicit leftSchema: Elem[R1], rightSchema: Elem[R2]) =
    new ExpPairTable[R1, R2](left, right)
  def unmkPairTable[R1:Elem, R2:Elem](p: Rep[PairTable[R1, R2]]) =
    Some((p.left, p.right))

  case class ExpShardedTable[R]
      (override val tableName: Rep[String], override val nShards: Rep[Int], override val distrib: Rep[R => Int], override val shards: Rep[Array[Table[R]]])
      (implicit schema: Elem[R], shardKeyPath: String)
    extends ShardedTable[R](tableName, nShards, distrib, shards) with UserTypeDef[BaseTable[R], ShardedTable[R]] {
    lazy val selfType = element[ShardedTable[R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpShardedTable[R](t(tableName), t(nShards), t(distrib), t(shards))
  }

  lazy val ShardedTable: Rep[ShardedTableCompanionAbs] = new ShardedTableCompanionAbs with UserTypeDef[ShardedTableCompanionAbs, ShardedTableCompanionAbs] {
    lazy val selfType = element[ShardedTableCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object ShardedTableMethods {
    object count {
      def unapply(d: Def[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insertFrom {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Arr[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(arr, _*)) if method.getName == "insertFrom" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, arr)).asInstanceOf[Option[(Rep[ShardedTable[R]], Arr[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Arr[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object filter {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => Boolean]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "filter" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => Boolean]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => Boolean]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Sum {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Sum" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Max {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Max" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Min {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Min" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object MapReduce {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = d match {
        case MethodCall(receiver, method, Seq(map, reduce, _*)) if method.getName == "MapReduce" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, map, reduce)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object GroupBy {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[R => G]) forSome {type R; type G}] = d match {
        case MethodCall(receiver, method, Seq(by, _*)) if method.getName == "GroupBy" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, by)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[R => G]) forSome {type R; type G}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[R => G]) forSome {type R; type G}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Join {
      def unapply(d: Def[_]): Option[(Rep[ShardedTable[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = d match {
        case MethodCall(receiver, method, Seq(inner, outKey, inKey, _*)) if method.getName == "Join" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some((receiver, inner, outKey, inKey)).asInstanceOf[Option[(Rep[ShardedTable[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedTable[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object gather {
      def unapply(d: Def[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "gather" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[ShardedTableElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedTable[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedTable[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object ShardedTableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[ShardedTableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[(Rep[String], Rep[Int], Rep[Int => Table[R]], Rep[R => Int]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(tableName, nShards, createShard, distrib, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[ShardedTableCompanionElem] =>
          Some((tableName, nShards, createShard, distrib)).asInstanceOf[Option[(Rep[String], Rep[Int], Rep[Int => Table[R]], Rep[R => Int]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[String], Rep[Int], Rep[Int => Table[R]], Rep[R => Int]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkShardedTable[R]
    (tableName: Rep[String], nShards: Rep[Int], distrib: Rep[R => Int], shards: Rep[Array[Table[R]]])(implicit schema: Elem[R], shardKeyPath: String) =
    new ExpShardedTable[R](tableName, nShards, distrib, shards)
  def unmkShardedTable[R:Elem](p: Rep[ShardedTable[R]]) =
    Some((p.tableName, p.nShards, p.distrib, p.shards))

  case class ExpShardedView[R]
      (override val nShards: Rep[Int], override val view: Rep[Int => Table[R]])
      (implicit schema: Elem[R], shardKeyPath: String)
    extends ShardedView[R](nShards, view) with UserTypeDef[BaseTable[R], ShardedView[R]] {
    lazy val selfType = element[ShardedView[R]].asInstanceOf[Elem[BaseTable[R]]]
    override def mirror(t: Transformer) = ExpShardedView[R](t(nShards), t(view))
  }

  lazy val ShardedView: Rep[ShardedViewCompanionAbs] = new ShardedViewCompanionAbs with UserTypeDef[ShardedViewCompanionAbs, ShardedViewCompanionAbs] {
    lazy val selfType = element[ShardedViewCompanionAbs]
    override def mirror(t: Transformer) = this
  }

  object ShardedViewMethods {
    object count {
      def unapply(d: Def[_]): Option[Rep[ShardedView[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedView[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedView[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insertFrom {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Arr[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(arr, _*)) if method.getName == "insertFrom" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, arr)).asInstanceOf[Option[(Rep[ShardedView[R]], Arr[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Arr[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object filter {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => Boolean]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "filter" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => Boolean]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => Boolean]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Sum {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Sum" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Max {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Max" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Min {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Min" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object MapReduce {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = d match {
        case MethodCall(receiver, method, Seq(map, reduce, _*)) if method.getName == "MapReduce" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, map, reduce)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object GroupBy {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[R => G]) forSome {type R; type G}] = d match {
        case MethodCall(receiver, method, Seq(by, _*)) if method.getName == "GroupBy" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, by)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[R => G]) forSome {type R; type G}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[R => G]) forSome {type R; type G}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Join {
      def unapply(d: Def[_]): Option[(Rep[ShardedView[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = d match {
        case MethodCall(receiver, method, Seq(inner, outKey, inKey, _*)) if method.getName == "Join" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some((receiver, inner, outKey, inKey)).asInstanceOf[Option[(Rep[ShardedView[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[ShardedView[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object gather {
      def unapply(d: Def[_]): Option[Rep[ShardedView[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "gather" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedView[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedView[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[ShardedView[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[ShardedViewElem[_]] =>
          Some(receiver).asInstanceOf[Option[Rep[ShardedView[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[ShardedView[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object ShardedViewCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[ShardedViewCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[(Rep[Int], Rep[Int => Table[R]]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(nShards, view, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[ShardedViewCompanionElem] =>
          Some((nShards, view)).asInstanceOf[Option[(Rep[Int], Rep[Int => Table[R]]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Int], Rep[Int => Table[R]]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  def mkShardedView[R]
    (nShards: Rep[Int], view: Rep[Int => Table[R]])(implicit schema: Elem[R], shardKeyPath: String) =
    new ExpShardedView[R](nShards, view)
  def unmkShardedView[R:Elem](p: Rep[ShardedView[R]]) =
    Some((p.nShards, p.view))

  object TableMethods {
    object tableName {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "tableName" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Select {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => P]) forSome {type R; type P}] = d match {
        case MethodCall(receiver, method, Seq(projection, _*)) if method.getName == "Select" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, projection)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => P]) forSome {type R; type P}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => P]) forSome {type R; type P}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object filter {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "filter" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object singleton {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "singleton" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Sum {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Sum" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Max {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Max" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Min {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Min" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Avg {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = d match {
        case MethodCall(receiver, method, Seq(agg, _*)) if method.getName == "Avg" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, agg)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => E]) forSome {type R; type E}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object count {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "count" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object MapReduce {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = d match {
        case MethodCall(receiver, method, Seq(map, reduce, _*)) if method.getName == "MapReduce" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, map, reduce)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => (K,V)], Rep[(V,V) => V]) forSome {type R; type K; type V}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object OrderBy {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => O]) forSome {type R; type O}] = d match {
        case MethodCall(receiver, method, Seq(by, _*)) if method.getName == "OrderBy" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, by)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => O]) forSome {type R; type O}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => O]) forSome {type R; type O}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object GroupBy {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => G]) forSome {type R; type G}] = d match {
        case MethodCall(receiver, method, Seq(by, _*)) if method.getName == "GroupBy" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, by)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => G]) forSome {type R; type G}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => G]) forSome {type R; type G}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Join {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = d match {
        case MethodCall(receiver, method, Seq(inner, outKey, inKey, _*)) if method.getName == "Join" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, inner, outKey, inKey)).asInstanceOf[Option[(Rep[Table[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[Table[I]], Rep[R => K], Rep[I => K]) forSome {type R; type I; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insert {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(record, _*)) if method.getName == "insert" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, record)).asInstanceOf[Option[(Rep[Table[R]], Rep[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object insertFrom {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Arr[R]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(arr, _*)) if method.getName == "insertFrom" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, arr)).asInstanceOf[Option[(Rep[Table[R]], Arr[R]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Arr[R]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object Delete {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(predicate, _*)) if method.getName == "Delete" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, predicate)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => Boolean]) forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object drop {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "drop" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object PrimaryKey {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "PrimaryKey" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object SecondaryKey {
      def unapply(d: Def[_]): Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}] = d match {
        case MethodCall(receiver, method, Seq(key, _*)) if method.getName == "SecondaryKey" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some((receiver, key)).asInstanceOf[Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[(Rep[Table[R]], Rep[R => K]) forSome {type R; type K}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object gather {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "gather" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object toArray {
      def unapply(d: Def[_]): Option[Rep[Table[R]] forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "toArray" && receiver.elem.isInstanceOf[TableElem[_, _, _]] =>
          Some(receiver).asInstanceOf[Option[Rep[Table[R]] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[Table[R]] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }

  object TableCompanionMethods {
    object defaultOf {
      def unapply(d: Def[_]): Option[Unit forSome {type R}] = d match {
        case MethodCall(receiver, method, _) if method.getName == "defaultOf" && receiver.elem.isInstanceOf[TableCompanionElem] =>
          Some(()).asInstanceOf[Option[Unit forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Unit forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }

    object create {
      def unapply(d: Def[_]): Option[Rep[String] forSome {type R}] = d match {
        case MethodCall(receiver, method, Seq(tableName, _*)) if method.getName == "create" && receiver.elem.isInstanceOf[TableCompanionElem] =>
          Some(tableName).asInstanceOf[Option[Rep[String] forSome {type R}]]
        case _ => None
      }
      def unapply(exp: Exp[_]): Option[Rep[String] forSome {type R}] = exp match {
        case Def(d) => unapply(d)
        case _ => None
      }
    }
  }
}
