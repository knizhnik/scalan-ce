package scalan.it.lms

import scalan.compilation.lms.LmsCompiler
import scalan.it.smoke.CommunitySmokeItTests
import scalan.parrays.PArraysDslExp
import scalan.compilation.GraphVizExport
import scalan.linalgebra.{MatricesDslExp, VectorsDslExp}
import scalan.collections._
import scalan.sql._
import scalan.community.{ScalanCommunityDslExp, ScalanCommunityExp}

class LmsSmokeItTests extends CommunitySmokeItTests {

  class ProgExp extends ProgCommunity with PArraysDslExp with MultiMapsDslExp with ScalanCommunityExp with GraphVizExport with LmsCompiler with VectorsDslExp with MatricesDslExp with SqlDslExp

  override val progStaged = new ProgExp

  test("test0simpleArith") {
    val in = 2
    compareOutputWithSequential(progStaged)(progSeq.simpleArith, progStaged.simpleArith, "simpleArith", in)
  }
  test("test1simpleArrGet") {
    val in = (Array(2, 3), 1)
    compareOutputWithSequential(progStaged)(progSeq.simpleArrGet, progStaged.simpleArrGet, "simpleArrGet", in)
  }
  test("test2simpleMap") {
    val in = Array(2, 3)
    compareOutputWithSequential(progStaged)(progSeq.simpleMap, progStaged.simpleMap, "simpleMap", in)
  }
  test("test3simpleMapNested") {
    val in = (Array(Array(2.0, 3.0), Array(3.0, 4.0)), 1)
    compareOutputWithSequential(progStaged)(progSeq.simpleMapNested, progStaged.simpleMapNested, "simpleMapNested", in)
  }
  test("test4simpleZip") {
    val in = Array(2, 3)
    compareOutputWithSequential(progStaged)(progSeq.simpleZip, progStaged.simpleZip, "simpleZip", in)
  }
  test("test5simpleZipWith") {
    val in = Array(2, 3)
    compareOutputWithSequential(progStaged)(progSeq.simpleZipWith, progStaged.simpleZipWith, "simpleZipWith", in)
  }
  test("test6simpleReduce") {
    val in = Array(2, 3)
    compareOutputWithSequential(progStaged)(progSeq.simpleReduce, progStaged.simpleReduce, "simpleArith", in)
  }
  test("test7mvMul") {
    val in = (Array(Array(2, 3), Array(4, 5)), Array(6, 7))
    compareOutputWithSequential(progStaged)(progSeq.mvMul, progStaged.mvMul, "mvMul", in)
  }
  /*
  test("test8expBaseArrays") {
    val in = Array(Array(2,3), Array(4,5))
    compareOutputWithSequential(progStaged)(progSeq.expBaseArrays, progStaged.expBaseArrays, "expBaseArrays", in)
  }
*/
  test("test9unionMaps") {
    val in = (Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5)), Array((0, 0.0), (2, 2.0), (4, 4.0), (6, 6.0)))
    compareOutputWithSequential(progStaged)(progSeq.unionMaps, progStaged.unionMaps, "unionMaps", in)
  }
  test("test10differenceMaps") {
    val in = (Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5)), Array((0, 0.0), (2, 2.0), (4, 4.0), (6, 6.0)))
    compareOutputWithSequential(progStaged)(progSeq.differenceMaps, progStaged.differenceMaps, "differenceMaps", in)
  }
  test("test11iterateMap") {
    val in = Array((1, 1.1), (2, 2.2), (3, 3.3))
    compareOutputWithSequential(progStaged)(progSeq.iterateMap, progStaged.iterateMap, "iterateMap", in)
  }
  test("test12mapReduce") {
    val in = Array(1, 2, 1, 1, 2, 3, 4, 1, 5, 4, 3, 2, 5, 2, 1)
    compareOutputWithSequential(progStaged)(progSeq.mapReduceByKey, progStaged.mapReduceByKey, "mapReduce", in)
  }
  test("test13filterCompound") {
    val in = Array((11, (12, 13)), (21, (22, 23)), (31, (32, 33)))
    compareOutputWithSequential(progStaged)(progSeq.filterCompound, progStaged.filterCompound, "filterCompound", in)
  }
  /* Not workign because of scalan/meta problem
  test("test13filterUDT") { 
    val in = Array((11, (12, 13)), (21, (22, 23)), (31, (32, 33)))
    compareOutputWithSequential(progStaged)(progSeq.filterUDT, progStaged.filterUDT, "filterUDT", in)
  }
  */
  test("test14aggregates") {
    val in = Array(1, 2, 3, 4, 5)
    compareOutputWithSequential(progStaged)(progSeq.aggregates, progStaged.aggregates, "aggregates", in)
  }
  test("test15sortBy") {
    val in = Array((2, 1), (3, 2), (1, 3), (5, 4), (4, 5))
    compareOutputWithSequential(progStaged)(progSeq.sortBy, progStaged.sortBy, "sortBy", in)
  }
  test("test15join") {
    val in = (Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5)), Array((0, 0.0), (2, 2.0), (4, 4.0), (6, 6.0)))
    compareOutputWithSequential(progStaged)(progSeq.joinMaps, progStaged.joinMaps, "joinMaps", in)
  }
  test("test16compoundMapKey") {
    val in = (Array((2, 1.0), (3, 2.0), (1, 3.0), (5, 4.0), (4, 5.0)), Array(1, 2, 3, 4, 5))
    compareOutputWithSequential(progStaged)(progSeq.compoundMapKey, progStaged.compoundMapKey, "compoundMapKey", in)
  }
  test("test17reduceMaps") {
    val in = (Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5)), Array((0, 0.0), (2, 2.0), (4, 4.0), (6, 6.0)))
    compareOutputWithSequential(progStaged)(progSeq.reduceMaps, progStaged.reduceMaps, "reduceMaps", in)
  }
  test("test18groupByCount") {
    val in = Array((2, 1), (3, 2), (2, 5), (1, 3), (5, 4), (1, 3), (4, 5), (2, 4))
    compareOutputWithSequential(progStaged)(progSeq.groupByCount, progStaged.groupByCount, "groupByCount", in)
  }
  test("test19groupBySum") {
    val in = Array((2, 1), (3, 2), (2, 5), (1, 3), (5, 4), (1, 3), (4, 5), (2, 4))
    compareOutputWithSequential(progStaged)(progSeq.groupBySum, progStaged.groupBySum, "groupBySum", in)
  }
  /*
  test("test20filterCompoundPArray") { 
    val in = Array((11, (12, 13)), (21, (22, 23)), (31, (32, 33)))
    compareOutputWithSequential(progStaged)(progSeq.filterCompoundPArray, progStaged.filterCompoundPArray, "filterCompoundPArray", in)
  }
  */
  test("test21compoundMapValue") {
    val in = (Array("one", "two", "three"), Array((1, 1.1), (2, 2.2), (3, 3.3)))
    compareOutputWithSequential(progStaged)(progSeq.compoundMapValue, progStaged.compoundMapValue, "filterCompound", in)
  }
  test("test22fillArrayBuffer") {
    val in = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    compareOutputWithSequential(progStaged)(progSeq.fillArrayBuffer, progStaged.fillArrayBuffer, "fillArrayBuffer", in)
  }
  test("test23unionMultiMaps") {
    val in = (Array((1, 1.1), (2, 2.2), (1, 3.3), (1, 4.4), (2, 5.5)), Array((0, 0.0), (2, 2.0), (1, 4.0), (1, 6.0)))
    compareOutputWithSequential(progStaged)(progSeq.unionMultiMaps, progStaged.unionMultiMaps, "unionMultiMaps", in)
  }

  test("test24simpleSelectTest") {
    val in = Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5))
    compareOutputWithSequential(progStaged)(progSeq.selectUsingIndex, progStaged.selectUsingIndex, "selectUsingIndex", in)
  }

  test("test25innerJoin") {
    val in = (Array((1, 1.0), (2, 0.0), (3, 1.0), (4, 1.0), (5, 0.0)), Array(("one", 1), ("two", 2), ("three", 3), ("four", 4), ("five", 5)))
    compareOutputWithSequential(progStaged)(progSeq.innerJoin, progStaged.innerJoin, "innerJoin", in)
  }

  test("test26hashJoin") {
    val in = (Array((1, 1.0), (2, 0.0), (3, 1.0), (4, 1.0), (5, 0.0)), Array(("one", 1), ("two", 2), ("three", 1), ("four", 4), ("five", 5)))
    compareOutputWithSequential(progStaged)(progSeq.hashJoin, progStaged.hashJoin, "hashJoin", in)
  }
  test("test27simpleIf") {
    val in = (Array(2.0,3.0), 4.0)
    compareOutputWithSequential(progStaged)(progSeq.simpleIf, progStaged.simpleIf, "simpleIf", in)
  }
  /*
  test("test27ifTest") {
    val in = (1, 0.0)
    compareOutputWithSequential(progStaged)(progSeq.ifTest, progStaged.ifTest, "ifTest", in)
  }
  */
  test("test28selectCount") {
    val in = Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5))
    compareOutputWithSequential(progStaged)(progSeq.selectCount, progStaged.selectCount, "selectCount", in)
  }
  test("test29sqlBenchmark") {
    val in = Array((1, "11243"), (2, "21235"), (3, "12343"), (4, "13455"), (5, "543123"))
    compareOutputWithSequential(progStaged)(progSeq.sqlBenchmark, progStaged.sqlBenchmark, "sqlBenchmark", in)
  }
  test("test30groupBy") {
    val in = Array((1, 1.1), (2, 2.2), (3, 3.3), (4, 4.4), (5, 5.5))
    compareOutputWithSequential(progStaged)(progSeq.groupBy, progStaged.groupBy, "groupBy", in)
  }
  test("test31columnarStore") {
    val in = Array((1, "11243"), (2, "21235"), (3, "12343"), (4, "13455"), (5, "543123"))
    compareOutputWithSequential(progStaged)(progSeq.columnarStore, progStaged.columnarStore, "columnarStore", in)
  }
  test("test32sqlParBenchmark") {
    val in = Array((1, "11243"), (2, "21235"), (3, "12343"), (4, "13455"), (5, "543123"))
    compareOutputWithSequential(progStaged)(progSeq.sqlParBenchmark, progStaged.sqlParBenchmark, "sqlParBenchmark", in)
  }
  test("test33reuseTest") {
    val in = 5
    compareOutputWithSequential(progStaged)(progSeq.reuseTest, progStaged.reuseTest, "reuseTest", in)
  }
  test("test34sqlIndexBenchmark") {
    val in = Array((1, "11243"), (2, "21235"), (3, "12343"), (4, "13455"), (5, "543123"))
    compareOutputWithSequential(progStaged)(progSeq.sqlIndexBenchmark, progStaged.sqlIndexBenchmark, "sqlIndexBenchmark", in)
  }
  test("test35sqlGroupBy") {
    val in = Array((1, "red"), (2, "green"), (3, "red"), (4, "blue"), (5, "blue"), (6, "green"), (7, "red"), (8, "blue"), (9, "red"))
    compareOutputWithSequential(progStaged)(progSeq.sqlGroupBy, progStaged.sqlGroupBy, "sqlGroupBy", in)
  }
  test("test36sqlMapReduce") {
    val in = (4, Array((1, "red"), (2, "green"), (3, "red"), (4, "blue"), (5, "blue"), (6, "green"), (7, "red"), (8, "blue"), (9, "red")))
    compareOutputWithSequential(progStaged)(progSeq.sqlMapReduce, progStaged.sqlMapReduce, "sqlMapReduce", in)
  }
  test("test37sqlParallelJoin") {
    val in = (Array((1, "red"), (2, "green"), (3, "blue")), (Array((1, "one"), (2, "two"), (3, "three")), Array((1, "true"), (2, "false"), (3, "unknown"))))
    compareOutputWithSequential(progStaged)(progSeq.sqlParallelJoin, progStaged.sqlParallelJoin, "sqlParallelJoin", in)
  }
  test("test38sqlAggJoin") {
    val in = (Array((1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)),
      (Array((1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)),
        (Array((1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)),
          (Array((1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)),
            (Array((1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)), 4)))))
    compareOutputWithSequential(progStaged)(progSeq.sqlAggJoin, progStaged.sqlAggJoin, "sqlAggJoin", in)
  }
  test("test39sqlIndexJoin") {
    val in = (Array((1, "red"), (2, "green"), (3, "blue")), Array((1, "one"), (2, "two"), (1, "three"), (1, "four"), (2, "five")))
    compareOutputWithSequential(progStaged)(progSeq.sqlIndexJoin, progStaged.sqlIndexJoin, "sqlIndexJoin", in)
  }
  test("test40sqlColumnarStoreBenchmark") {
    val in = (4, Array((1, "11243"), (2, "21235"), (3, "12343"), (4, "13455"), (5, "543123")))
    compareOutputWithSequential(progStaged)(progSeq.sqlColumnarStoreBenchmark, progStaged.sqlColumnarStoreBenchmark, "sqlColumnarStoreBenchmark", in)
  }
  test("testTuple") {
    val in = (Array((1, "red"), (2, "green"), (3, "blue")), Array((1, "one"), (2, "two"), (3, "three")))
    compareOutputWithSequential(progStaged)(progSeq.testTuple, progStaged.testTuple, "testTuple", in)
  }
}