package scalan.compilation.lms

import java.util.HashMap
import scalan.community.ScalanCommunityExp
import scalan.linalgebra.VectorsDslExp
import scalan.collections._

abstract class LmsBridge[A,B] {
  val scalan: ScalanCommunityExp with LmsCompiler with VectorsDslExp // TODO remove this!

  def outerApply[Ctx <: scalan.Transformer](lFunc: LmsFunction[A,B])(in: lFunc.Exp[A], g: scalan.ProgramGraph[Ctx]): lFunc.Exp[B] = {

    import scala.collection.Map

    val emptySymMirror = Map.empty[scalan.Exp[_], lFunc.Exp[X] forSome {type X}]
    val emptyFuncMirror = Map.empty[scalan.Exp[_], ( lFunc.Exp[X] => lFunc.Exp[Y]) forSome {type X; type Y}]

    val definitions = g.schedule

    type SymMirror = Map[scalan.Exp[_], lFunc.Exp[A] forSome {type A}]

    type FuncMirror = Map[scalan.Exp[_],( lFunc.Exp[A] => lFunc.Exp[B]) forSome { type A; type B } ]

    def mirrorLambdaToLmsFunc[I, R](lam:scalan.Lambda[I,R],
                                    symMirror: SymMirror,
                                    funcMirror: FuncMirror) : (lFunc.Exp[I] => lFunc.Exp[R]) = {
      val lamX = lam.x
      val f = { x: lFunc.Exp[I] =>
        val sched = lam.scheduleSingleLevel
        val (lamExps, _, _) = mirrorDefs(lam, sched, symMirror + ((lamX, x)), funcMirror )
        val res = lamExps.lastOption.getOrElse(x)
        res.asInstanceOf[lFunc.Exp[R]]
      }
      f
    }

    /* Mirror block */
    def mirrorBlockToLms[R](block:scalan.DefBlock,
                            symMirror: SymMirror,
                            funcMirror: FuncMirror,
                            dflt: scalan.Rep[_]) = () => {
      val sched = block.scheduleSingleLevel
      val (blockExps, _, _) = mirrorDefs(block, sched, symMirror, funcMirror )
      val res = blockExps.lastOption.getOrElse(symMirror(dflt))
      res.asInstanceOf[lFunc.Exp[R]]
    }

    /*
    def mirrorSym(symMirror: SymMirror, sym: scalan.Exp[_]) = {
      sym match {
        case (scalan.Def(buf: scalan.ArrayBufferEmpty[_])) => {
          buf.selfType match {
            case elem: scalan.ArrayBufferElem[t] => {
              scalan.createManifest(elem.eItem) match {
                case mT: Manifest[t] => {
                  lFunc.emptyArrayBuffer[t]()(mT)
                }
              }
            }
          }
        }
        case scalan.Def(map: scalan.EmptyMap[_, _]) => {
          map.selfType match {
            case elem: scalan.PMapElem[k, v] => {
              (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                case (mK: Manifest[k], mV: Manifest[v]) => {
                  lFunc.emptyMap[k, v]()(mK, mV)
                }
              }
            }
          }
        }
        case scalan.Def(buf: scalan.ArrayBufferUsingFunc[_]) => {
          buf.selfType match {
            case elem: scalan.ArrayBufferElem[t] => {
              scalan.createManifest(elem.eItem) match {
                case mT: Manifest[t] => {
                  lFunc.emptyArrayBuffer[t]()(mT)
                }
              }
            }
          }
        }
        case scalan.Def(map: scalan.MapUsingFunc[_, _]) => {
          map.selfType match {
            case elem: scalan.PMapElem[k, v] => {
              (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                case (mK: Manifest[k], mV: Manifest[v]) => {
                  lFunc.emptyMap[k, v]()(mK, mV)
                }
              }
            }
          }
        }
        case _ => symMirror(sym)
      }
    }
    */
    
    def mirrorDefs(fromGraph: scalan.AstGraph, defs: Seq[scalan.TableEntry[_]], symMirror: SymMirror, funcMirror: FuncMirror):
    (Seq[lFunc.Exp[_]], SymMirror, FuncMirror) =
    {
      val (lmsExps, finalSymMirr, finalFuncMirr) = defs.foldLeft(List.empty[lFunc.Exp[_]], symMirror, funcMirror) {
        case ((exps, symMirr, funcMirr),tp) => {
          val s = tp.sym
          tp.rhs match {
            case lam: scalan.Lambda[_, _] => {
              (scalan.createManifest(lam.eA), scalan.createManifest(lam.eB)) match {
                case (mA: Manifest[a], mB: Manifest[b]) => {
                  val f = mirrorLambdaToLmsFunc[a, b](lam.asInstanceOf[scalan.Lambda[a, b]], symMirr, funcMirr)
                  val exp = lFunc.lambda(f)(mA, mB)
                  (exps, symMirr + ((s, exp)), funcMirr + ((s, f)))
                }
              }
            }
            case c@scalan.Const(_) => {
              val exp = lFunc.unitD(c.x)
              (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
            }
            case scalan.Tup(l, r) => {
              (scalan.createManifest(l.elem), scalan.createManifest(r.elem)) match {
                case (mA: Manifest[a], mB: Manifest[b]) => {
                  val l_ = symMirr(l).asInstanceOf[lFunc.Exp[a]]
                  val r_ = symMirr(r).asInstanceOf[lFunc.Exp[b]]
                  val exp = lFunc.tuple[a, b](l_, r_)(mA, mB)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                }
              }
            }
            case scalan.First(tuple) => {
              tuple.elem match {
                case pe: scalan.PairElem[_, _] =>
                  (scalan.createManifest(pe.eFst), scalan.createManifest(pe.eSnd)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val tup = symMirr(tuple).asInstanceOf[lFunc.Exp[(a, b)]]
                      val exp = lFunc.first[a, b](tup)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }
            case scalan.Second(tuple) => {
              tuple.elem match {
                case pe: scalan.PairElem[_, _] =>
                  (scalan.createManifest(pe.eFst), scalan.createManifest(pe.eSnd)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val tup = symMirr(tuple).asInstanceOf[lFunc.Exp[(a, b)]]
                      val exp = lFunc.second[a, b](tup)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }
            case scalan.ApplyUnOp(op, arg1) => {
              scalan.createManifest(arg1.elem) match {
                case (mA: Manifest[a]) => {
                  val arg1_ = symMirr(arg1).asInstanceOf[lFunc.Exp[a]]
                  val exp:lFunc.Exp[_] = op.asInstanceOf[scalan.UnOp[a, _]] match {
                    case scalan.Not => lFunc.Not(arg1_.asInstanceOf[lFunc.Exp[Boolean]])
                    case scalan.NumericNegate(n)  => lFunc.Neg(arg1_)(mA, n.asInstanceOf[Numeric[a]])
                    case scalan.NumericToDouble(n)  =>
                      mA match {
                        case Manifest.Int => lFunc.IntToDouble(arg1_.asInstanceOf[lFunc.Exp[Int]])
                        case Manifest.Float => lFunc.FloatToDouble(arg1_.asInstanceOf[lFunc.Exp[Float]])
                       }
                     case scalan.NumericToFloat(n)  =>
                       mA match {
                         case Manifest.Int  => lFunc.IntToFloat(arg1_.asInstanceOf[lFunc.Exp[Int]])
                         case Manifest.Double => lFunc.DoubleToFloat(arg1_.asInstanceOf[lFunc.Exp[Double]])
                       }
                    case scalan.NumericToInt(n)  =>
                      mA match {
                        case Manifest.Float => lFunc.FloatToInt(arg1_.asInstanceOf[lFunc.Exp[Float]])
                        case Manifest.Double => lFunc.DoubleToInt(arg1_.asInstanceOf[lFunc.Exp[Double]])
                       }
                    case scalan.NumericToString()  => lFunc.ToString(arg1_)
                    case scalan.HashCode() => lFunc.hashCode(arg1_)
                  }
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                }
              }
            }
            case scalan.ApplyBinOp(op, arg1, arg2) => {
              scalan.createManifest(arg1.elem) match {
                case (mA: Manifest[a]) =>
                  val arg1_ = symMirr(arg1).asInstanceOf[lFunc.Exp[a]]
                  val arg2_ = symMirr(arg2).asInstanceOf[lFunc.Exp[a]]
                  val exp = op.asInstanceOf[scalan.BinOp[a, _]] match {
                    case scalan.NumericTimes(n) =>
                      lFunc.opMult(arg1_, arg2_)(n.asInstanceOf[Numeric[a]], mA)
                    case scalan.NumericPlus(n) =>
                      lFunc.opPlus(arg1_, arg2_)(n.asInstanceOf[Numeric[a]], mA)
                    case scalan.NumericMinus(n) =>
                      lFunc.opMinus(arg1_, arg2_)(n.asInstanceOf[Numeric[a]], mA)
                    case scalan.IntegralDivide(n) =>
                      lFunc.opDiv(arg1_, arg2_)(n.asInstanceOf[Numeric[a]], mA)
                    case scalan.IntegralMod(n) =>
                      if (mA == Manifest.Int)
                        lFunc.opMod(arg1_.asInstanceOf[lFunc.Exp[Int]], arg2_.asInstanceOf[lFunc.Exp[Int]])
                      else
                        throw new IllegalStateException(s"LMS only supports mod operation for Int, got $mA instead")
                    case scalan.FractionalDivide(n) =>
                      lFunc.opDiv(arg1_, arg2_)(n.asInstanceOf[Numeric[a]], mA)
                    case scalan.Equals() =>
                      lFunc.opEq[a](arg1_, arg2_)(mA)
                    case scalan.OrderingLT(ord) =>
                      lFunc.LT[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.OrderingLTEQ(ord) =>
                      lFunc.LTEQ[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.OrderingGT(ord) =>
                      lFunc.GT[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.OrderingGTEQ(ord) =>
                      lFunc.GTEQ[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.OrderingMax(ord) =>
                      lFunc.Max[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.OrderingMin(ord) =>
                      lFunc.Min[a](arg1_, arg2_)(mA, ord.asInstanceOf[Ordering[a]])
                    case scalan.And =>
                      lFunc.And(arg1_.asInstanceOf[lFunc.Exp[Boolean]], arg2_.asInstanceOf[lFunc.Exp[Boolean]])
                    case scalan.Or =>
                      lFunc.Or(arg1_.asInstanceOf[lFunc.Exp[Boolean]], arg2_.asInstanceOf[lFunc.Exp[Boolean]])
                    case scalan.StringConcat() =>
                      lFunc.stringConcat(arg1_.asInstanceOf[lFunc.Exp[String]], arg2_.asInstanceOf[lFunc.Exp[String]])
                    case scalan.StringContains() =>
                      lFunc.stringContains(arg1_.asInstanceOf[lFunc.Exp[String]], arg2_.asInstanceOf[lFunc.Exp[String]])
                    case scalan.StringStartsWith() =>
                      lFunc.stringStartsWith(arg1_.asInstanceOf[lFunc.Exp[String]], arg2_.asInstanceOf[lFunc.Exp[String]])
                    case scalan.StringEndsWith() =>
                      lFunc.stringEndsWith(arg1_.asInstanceOf[lFunc.Exp[String]], arg2_.asInstanceOf[lFunc.Exp[String]])
                     case scalan.StringMatches() =>
                      lFunc.stringMatches(arg1_.asInstanceOf[lFunc.Exp[String]], arg2_.asInstanceOf[lFunc.Exp[String]])
                 }
                  (exps ++ List(exp), symMirr + ((s,exp)), funcMirr )
              }
            }

            case scalan.Block(left, right) => {
              (scalan.createManifest(left.elem), scalan.createManifest(right.elem)) match {
                case (mA: Manifest[a], mB: Manifest[b]) => {
                  val left_ = symMirr(left).asInstanceOf[lFunc.Exp[a]]
                  val right_ = symMirr(right).asInstanceOf[lFunc.Exp[b]]
                  val exp = lFunc.block(left_, right_)(mA, mB)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                }
              }
            }
            case scalan.ThrowException(msg) => {
              val msg_ = symMirr(msg).asInstanceOf[lFunc.Exp[String]]
              val exp = lFunc.throwException(msg_)
              (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
            }
            case i@scalan.IfThenElse(cond, iftrue, iffalse) => {
              scalan.createManifest(i.selfType) match {
                case (mA: Manifest[a]) => {
                  val cond_ = symMirr(cond).asInstanceOf[lFunc.Exp[Boolean]]
                  fromGraph.branches.ifBranches.get(s) match {
                    case Some(b) => {
                      def thenBody = mirrorBlockToLms(b.thenBody, symMirr, funcMirr, iftrue)
                      def elseBody = mirrorBlockToLms(b.elseBody, symMirr, funcMirr, iffalse)
                      val exp = lFunc.ifThenElse(cond_, thenBody , elseBody)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                    case _ => {
                      val then_ = symMirr(iftrue).asInstanceOf[lFunc.Exp[a]]
                      val else_ = symMirr(iffalse).asInstanceOf[lFunc.Exp[a]]
                      val exp = lFunc.ifThenElse(cond_, () => then_ , () => else_)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case loop@scalan.LoopUntil(state, stepSym@scalan.Def(step: scalan.Lambda[_, _]), condSym@scalan.Def(cond: scalan.Lambda[_, _])) => {
              scalan.createManifest(loop.selfType) match {
                case (mA: Manifest[a]) => {
                  val cond_ = mirrorLambdaToLmsFunc[a, Boolean](cond.asInstanceOf[scalan.Lambda[a, Boolean]], symMirr, funcMirr)
                  val step_ = mirrorLambdaToLmsFunc[a, a](step.asInstanceOf[scalan.Lambda[a, a]], symMirr, funcMirr)
                  val state_ = symMirr(state).asInstanceOf[lFunc.Exp[a]]
                  val exp = lFunc.loopUntil[a](state_, cond_, step_)(mA)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((condSym, cond_)) + ((stepSym, step_)))
                }
              }
            }
            //
            // Array Buffer
            //
            case buf@scalan.ArrayBufferEmpty() => {
              buf.selfType match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val exp = lFunc.emptyArrayBuffer[t]()(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case buf@scalan.MakeArrayBuffer(ctx) => {
              buf.selfType match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val exp = lFunc.emptyArrayBuffer[t]()(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case buf@scalan.ArrayBufferFromElem(e) => {
              buf.selfType match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val v = symMirr(e).asInstanceOf[lFunc.Exp[t]]
                      val exp = lFunc.arrayBufferFromElem(v)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferApply(buf, i) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val i_ = symMirr(i).asInstanceOf[lFunc.Exp[Int]]
                      val exp = lFunc.arrayBufferApply(b_, i_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferLength(buf) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val exp = lFunc.arrayBufferLength(b_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case res@scalan.ArrayBufferMap(buf, lambdaSym@scalan.Def(lambda: scalan.Lambda[_, _])) => {
              (res.selfType, buf.elem) match {
                case (from: scalan.ArrayBufferElem[a], to: scalan.ArrayBufferElem[b]) => {
                  (scalan.createManifest(from.eItem), scalan.createManifest(to.eItem)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[a]]]
                      val f_ = mirrorLambdaToLmsFunc[a, b](lambda.asInstanceOf[scalan.Lambda[a, b]], symMirr, funcMirr)
                      val exp = lFunc.arrayBufferMap(b_, f_)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f_)))
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferUpdate(buf, i, v) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val i_ = symMirr(i).asInstanceOf[lFunc.Exp[Int]]
                      val v_ = symMirr(v).asInstanceOf[lFunc.Exp[t]]
                      val exp = lFunc.arrayBufferUpdate(b_, i_, v_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferInsert(buf, i, v) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val i_ = symMirr(i).asInstanceOf[lFunc.Exp[Int]]
                      val v_ = symMirr(v).asInstanceOf[lFunc.Exp[t]]
                      val exp = lFunc.arrayBufferInsert(b_, i_, v_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferAppend(buf, v) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val v_ = symMirr(v).asInstanceOf[lFunc.Exp[t]]
                      val exp = lFunc.arrayBufferAppend(b_, v_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferAppendArray(buf, a) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val a_ = symMirr(a).asInstanceOf[lFunc.Exp[Array[t]]]
                      val exp = lFunc.arrayBufferAppendArray(b_, a_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferRemove(buf, i, n) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val i_ = symMirr(i).asInstanceOf[lFunc.Exp[Int]]
                      val n_ = symMirr(n).asInstanceOf[lFunc.Exp[Int]]
                      val exp = lFunc.arrayBufferRemove(b_, i_, n_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferReset(buf) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val exp = lFunc.arrayBufferReset(b_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayBufferToArray(buf) => {
              buf.elem match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val b_ = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                      val exp = lFunc.arrayBufferToArray(b_)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case ab@scalan.ArrayBufferRep(buf) => {
              ab.selfType match {
                case elem: scalan.ArrayBufferElem[t] => {
                  val exp = symMirr(buf).asInstanceOf[lFunc.Exp[scala.collection.mutable.ArrayBuilder[t]]]
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                }
              }
            }
            case buf@scalan.ArrayBufferUsingFunc(count, lambdaSym@scalan.Def(lambda:scalan.Lambda[_,_])) => {
              buf.selfType match {
                case elem: scalan.ArrayBufferElem[t] => {
                  scalan.createManifest(elem.eItem) match {
                    case mT: Manifest[t] => {
                      val n = symMirr(count).asInstanceOf[lFunc.Exp[Int]]
                      val f = mirrorLambdaToLmsFunc[Int, t](lambda.asInstanceOf[scalan.Lambda[Int, t]], symMirr, funcMirr)
                      val exp = lFunc.arrayBufferUsingFunc(n, f)(mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                    }
                  }
                }
              }
            }


            //
            // Map
            //
            case scalan.AppendMultiMap(map, key, value) => {
              (key.elem, value.elem) match {
                case (eK: scalan.Elem[k], eV: scalan.Elem[v]) => {
                  (scalan.createManifest(eK), scalan.createManifest(eV)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, scala.collection.mutable.ArrayBuilder[v]]]]
                      val key_ = symMirr(key).asInstanceOf[lFunc.Exp[k]]
                      val value_ = symMirr(value).asInstanceOf[lFunc.Exp[v]]
                      val exp = lFunc.multiMapAppend(map_, key_, value_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)

                    }
                  }
                }
              }
            }
            case pm@scalan.VarPM(map) => {
              pm.selfType match {
                case elem: scalan.PMapElem[k, v] => {
                  val exp = symMirr(map).asInstanceOf[lFunc.Exp[scalan.PMap[k, v]]]
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                }
              }
            }

            case map@scalan.MapFromArray(arr) => {
              map.selfType match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val arr_ = symMirr(arr).asInstanceOf[lFunc.Exp[Array[(k, v)]]]
                      val exp = lFunc.mapFromArray[k, v](arr_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case map@scalan.MapUsingFunc(count, lambdaSym@scalan.Def(lambda:scalan.Lambda[_,_])) => {
              map.selfType match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val n = symMirr(count).asInstanceOf[lFunc.Exp[Int]]
                      val f = mirrorLambdaToLmsFunc[Int, (k, v)](lambda.asInstanceOf[scalan.Lambda[Int, (k, v)]], symMirr, funcMirr)
                      val exp = lFunc.mapUsingFunc[k, v](n, f)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                    }
                  }
                }
              }
            }
            case map@scalan.EmptyMap() => {
              map.selfType match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val exp = lFunc.emptyMap[k, v]()(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case map@scalan.MakeMap(ctx) => {
              map.selfType match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val exp = lFunc.emptyMap[k, v]()(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapUnion(left, right) => {
              left.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val left_ = symMirr(left).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val right_ = symMirr(right).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapUnion[k, v](left_, right_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapDifference(left, right) => {
              left.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val left_ = symMirr(left).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val right_ = symMirr(right).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapDifference[k, v](left_, right_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case tk@scalan.MapTransformValues(map, sym@scalan.Def(lambda: scalan.Lambda[_, _])) => {
              (map.elem, tk.selfType) match {
                case (in: scalan.PMapElem[k, v], out: scalan.PMapElem[_, t]) => {
                  (scalan.createManifest(in.eKey), scalan.createManifest(in.eValue), scalan.createManifest(out.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v], mT: Manifest[t]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val f = mirrorLambdaToLmsFunc[v, t](lambda.asInstanceOf[scalan.Lambda[v, t]], symMirr, funcMirr)
                      val exp = lFunc.mapTransformValues[k, v, t](map_, f)(mK, mV, mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((sym, f)))
                    }
                  }
                }
              }
            }
            case scalan.MapReduce(left, right, reduceSym@scalan.Def(reduce: scalan.Lambda[_, _])) => {
              left.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val left_ = symMirr(left).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val right_ = symMirr(right).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val reduce_ = mirrorLambdaToLmsFunc[(v, v), v](reduce.asInstanceOf[scalan.Lambda[(v, v), v]], symMirr, funcMirr)
                      val exp = lFunc.mapReduce[k, v](left_, right_, reduce_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((reduceSym, reduce_)))
                    }
                  }
                }
              }
            }
            case scalan.MapJoin(left, right) => {
              (left.elem, right.elem) match {
                case (e1: scalan.PMapElem[k1, v1], e2: scalan.PMapElem[k2, v2]) => {
                  (scalan.createManifest(e1.eKey), scalan.createManifest(e1.eValue), scalan.createManifest(e2.eValue)) match {
                    case (mK: Manifest[k1], mV1: Manifest[v1], mV2: Manifest[v2]) => {
                      val left_ = symMirr(left).asInstanceOf[lFunc.Exp[HashMap[k1, v1]]]
                      val right_ = symMirr(right).asInstanceOf[lFunc.Exp[HashMap[k1, v2]]]
                      val exp = lFunc.mapJoin[k1, v1, v2](left_, right_)(mK, mV1, mV2)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapContains(map, key) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val key_ = symMirr(key).asInstanceOf[lFunc.Exp[k]]
                      val exp = lFunc.mapContains[k, v](map_, key_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapApply(map, key) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val key_ = symMirr(key).asInstanceOf[lFunc.Exp[k]]
                      val exp = lFunc.mapApply[k, v](map_, key_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case m@scalan.MapApplyIf(map, key, s1@scalan.Def(exists:scalan.Lambda[_,_]), s2@scalan.Def(otherwise:scalan.Lambda[_,_])) => {
              (m.selfType, map.elem) match {
                case (eT: scalan.Elem[t], eM: scalan.PMapElem[k, v]) => {
                  (scalan.createManifest(eT), scalan.createManifest(eM.eKey), scalan.createManifest(eM.eValue)) match {
                    case (mT: Manifest[t], mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val key_ = symMirr(key).asInstanceOf[lFunc.Exp[k]]
                      val exists_ = mirrorLambdaToLmsFunc[v, t](exists.asInstanceOf[scalan.Lambda[v, t]], symMirr, funcMirr)
                      val otherwise_ = mirrorLambdaToLmsFunc[Unit, t](otherwise.asInstanceOf[scalan.Lambda[Unit, t]], symMirr, funcMirr)
                      val exp = lFunc.mapApplyIf[k, v, t](map_, key_, exists_, otherwise_)(mK, mV, mT)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((s1, exists_)) + ((s2, otherwise_)))
                    }
                  }
                }
              }
            }
            case scalan.MapUpdate(map, key, value) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val key_ = symMirr(key).asInstanceOf[lFunc.Exp[k]]
                      val value_ = symMirr(value).asInstanceOf[lFunc.Exp[v]]
                      val exp = lFunc.mapUpdate[k, v](map_, key_, value_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapSize(map) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapSize[k, v](map_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapToArray(map) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapToArray[k, v](map_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapKeys(map) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapKeys[k, v](map_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.MapValues(map) => {
              map.elem match {
                case elem: scalan.PMapElem[k, v] => {
                  (scalan.createManifest(elem.eKey), scalan.createManifest(elem.eValue)) match {
                    case (mK: Manifest[k], mV: Manifest[v]) => {
                      val map_ = symMirr(map).asInstanceOf[lFunc.Exp[HashMap[k, v]]]
                      val exp = lFunc.mapValues[k, v](map_)(mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }

            case apply@scalan.ArrayApply(xs, ind) => {
              scalan.createManifest(apply.selfType) match {
                case (mA: Manifest[a]) =>
                  val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val ind_ = symMirr(ind).asInstanceOf[lFunc.Exp[Int]]
                  val exp = lFunc.arrayGet[a](xs_, ind_)(mA)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            }
            case scalan.ArrayApplyMany(xs, idxs) => {
              (xs.elem) match {
                case (el: scalan.ArrayElem[_]) =>
                  scalan.createManifest(el.eItem) match {
                    case (mA: Manifest[a]) =>
                      val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                      val idxs_ = symMirr(idxs).asInstanceOf[lFunc.Exp[Array[Int]]]
                      val exp = lFunc.arrayGather[a](xs_, idxs_)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }
            case scalan.ArrayLength(xs) => {
              scalan.createManifest(xs.elem) match {
                case (mA: Manifest[a]) =>
                  val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val exp = lFunc.arrayLength[a](xs_)(mA)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            }
            case scalan.ArrayRangeFrom0(n) => {
              val n_ = symMirr(n).asInstanceOf[lFunc.Exp[Int]]
              val exp = lFunc.indexRangeD(n_)
              (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
            }
            case scalan.ArraySort(arg, o) => {
              arg.elem match {
                case (el: scalan.ArrayElem[_]) =>
                  scalan.createManifest(el.eItem) match {
                    case (mA: Manifest[a]) =>
                      val arg_ = symMirr(arg).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.arraySort[a](arg_)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }
            case sort@scalan.ArraySortBy(arg, lambdaSym@scalan.Def(by: scalan.Lambda[_, b]), o) => {
              sort.selfType match {
                case (el: scalan.ArrayElem[a]) => {
                  (scalan.createManifest(el.eItem), scalan.createManifest(by.eB)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val by_ = mirrorLambdaToLmsFunc[a, b](by.asInstanceOf[scalan.Lambda[a, b]], symMirr, funcMirr)
                      val arg_ = symMirr(arg).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.arraySortBy[a, b](arg_, by_)(mA, mB, o.asInstanceOf[Ordering[b]])
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, by_)))
                  }
                }
              }
            }
            case gby@scalan.ArrayGroupBy(arg, lambdaSym@scalan.Def(by: scalan.Lambda[_, _])) => {
              (arg.elem, gby.selfType) match {
                case (ae: scalan.ArrayElem[a], me: scalan.PMapElem[k, v]) => {
                  (scalan.createManifest(ae.eItem), scalan.createManifest(me.eKey)) match {
                    case (mA: Manifest[a], mK: Manifest[k]) => {
                      val by_ = mirrorLambdaToLmsFunc[a, k](by.asInstanceOf[scalan.Lambda[a, k]], symMirr, funcMirr)
                      val arg_ = symMirr(arg).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.arrayGroupBy[a, k](arg_, by_)(mA, mK)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, by_)))
                    }
                  }
                }
              }
            }
            case sum@scalan.ArraySum(xs, n) => {
              scalan.createManifest(sum.selfType) match {
                case (mA: Manifest[a]) =>
                  val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val exp = lFunc.arraySum[a](xs_)(mA, n.asInstanceOf[Numeric[a]])
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            }
            case min@scalan.ArrayMin(xs, o) => {
              scalan.createManifest(min.selfType) match {
                case (mA: Manifest[a]) =>
                  val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val exp = lFunc.arrayMin[a](xs_)(mA, o.asInstanceOf[Ordering[a]])
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            }
            case max@scalan.ArrayMax(xs, o) => {
              scalan.createManifest(max.selfType) match {
                case (mA: Manifest[a]) =>
                  val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val exp = lFunc.arrayMax[a](xs_)(mA, o.asInstanceOf[Ordering[a]])
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            }
            case scalan.ArrayAvg(xs, n) => {
              xs.elem match {
                case (el: scalan.ArrayElem[a]) => {
                  scalan.createManifest(el.eItem) match {
                    case (mA: Manifest[a]) => {
                      val xs_ = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.arrayAvg[a](xs_)(mA, n.asInstanceOf[Numeric[a]])
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                    }
                  }
                }
              }
            }
            case scalan.ArrayZip(arg1, arg2) => {
              (arg1.elem, arg2.elem) match {
                case (el1: scalan.ArrayElem[_], el2: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el1.eItem), scalan.createManifest(el2.eItem)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val arg1_ = symMirr(arg1).asInstanceOf[lFunc.Exp[Array[a]]]
                      val arg2_ = symMirr(arg2).asInstanceOf[lFunc.Exp[Array[b]]]
                      val exp = lFunc.opZip[a, b](arg1_, arg2_)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }
            case map@scalan.ArrayMap(source, lambdaSym@scalan.Def(lam: scalan.Lambda[_, _])) => {
              (source.elem, map.selfType) match {
                case (el: scalan.ArrayElem[_], el1: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el.eItem), scalan.createManifest(el1.eItem)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val f = mirrorLambdaToLmsFunc[a, b](lam.asInstanceOf[scalan.Lambda[a, b]], symMirr, funcMirr) //(mA, mB)
                      val lmsSource = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.mapArray[a, b](lmsSource, f)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                  }
              }
            }
            case map@scalan.ArrayFlatMap(source, lambdaSym@scalan.Def(lam: scalan.Lambda[_, _])) => {
              (source.elem, map.selfType) match {
                case (el: scalan.ArrayElem[_], el1: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el.eItem), scalan.createManifest(el1.eItem)) match {
                    case (mA: Manifest[a], mB: Manifest[b]) =>
                      val f = mirrorLambdaToLmsFunc[a, Array[b]](lam.asInstanceOf[scalan.Lambda[a, Array[b]]], symMirr, funcMirr) //(mA, mB)
                      val lmsSource = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.flatMapArray[a, b](lmsSource, f)(mA, mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                  }
              }
            }
            case filter@scalan.ArrayFilter(source, lambdaSym@scalan.Def(lam: scalan.Lambda[_, _])) => {
              (filter.selfType) match {
                case (el: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el.eItem)) match {
                    case (mA: Manifest[a]) =>
                      val f = mirrorLambdaToLmsFunc[a, Boolean](lam.asInstanceOf[scalan.Lambda[a, Boolean]], symMirr, funcMirr) //(mA, mB)
                      val lmsSource = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.filterArray[a](lmsSource, f)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                  }
              }
            }
            case find@scalan.ArrayFind(source, lambdaSym@scalan.Def(lam: scalan.Lambda[_, _])) => {
              (source.elem) match {
                case (el: scalan.ArrayElem[_]) => {
                  (scalan.createManifest(el.eItem)) match {
                    case (mA: Manifest[a]) =>
                      val f = mirrorLambdaToLmsFunc[a, Boolean](lam.asInstanceOf[scalan.Lambda[a, Boolean]], symMirr, funcMirr) //(mA, mB)
                    val lmsSource = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.findArray[a](lmsSource, f)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                  }
                }
              }
            }
            case scalan.ArrayCount(source, lambdaSym@scalan.Def(lam: scalan.Lambda[_, _])) => {
              (source.elem) match {
                case (el: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el.eItem)) match {
                    case (mA: Manifest[a]) =>
                      val f = mirrorLambdaToLmsFunc[a, Boolean](lam.asInstanceOf[scalan.Lambda[a, Boolean]], symMirr, funcMirr) //(mA, mB)
                      val lmsSource = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.countArray[a](lmsSource, f)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((lambdaSym, f)))
                  }
              }
            }
            case mr@scalan.ArrayMapReduce(source, mapSym@scalan.Def(map: scalan.Lambda[_, _]), reduceSym@scalan.Def(reduce: scalan.Lambda[_, _])) => {
              (source.elem, mr.selfType) match {
                case (ae: scalan.ArrayElem[a], me: scalan.PMapElem[k, v]) => {
                  (scalan.createManifest(ae.eItem), scalan.createManifest(me.eKey), scalan.createManifest(me.eValue)) match {
                    case (mA: Manifest[a], mK: Manifest[k], mV: Manifest[v]) => {
                      val source_ = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val map_ = mirrorLambdaToLmsFunc[a, (k, v)](map.asInstanceOf[scalan.Lambda[a, (k, v)]], symMirr, funcMirr)
                      val reduce_ = mirrorLambdaToLmsFunc[(v, v), v](reduce.asInstanceOf[scalan.Lambda[(v, v), v]], symMirr, funcMirr)
                      val exp = lFunc.arrayMapReduce(source_, map_, reduce_)(mA, mK, mV)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((mapSym, map_)) + ((reduceSym, reduce_)))
                    }
                  }
                }
              }
            }
            case f@scalan.ArrayFold(source, init, sym@scalan.Def(step: scalan.Lambda[_, _])) => {
              (f.selfType, source.elem) match {
                case (e: scalan.Elem[s], ae: scalan.ArrayElem[a]) => {
                  (scalan.createManifest(e), scalan.createManifest(ae.eItem)) match {
                    case (mS: Manifest[s], mA: Manifest[a]) => {
                      val src = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val state = symMirr(init).asInstanceOf[lFunc.Exp[s]]
                      val func = mirrorLambdaToLmsFunc[(s, a), s](step.asInstanceOf[scalan.Lambda[(s, a), s]], symMirr, funcMirr)
                      val exp = lFunc.fold[a, s](src, state, func)(mA, mS)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((sym, func)))
                    }
                  }
                }
              }
            }
            case sum@scalan.ArraySumBy(source, sym@scalan.Def(f: scalan.Lambda[_, _]), n) => {
              (sum.selfType, source.elem) match {
                case (e: scalan.Elem[s], ae: scalan.ArrayElem[a]) => {
                  (scalan.createManifest(e), scalan.createManifest(ae.eItem)) match {
                    case (mS: Manifest[s], mA: Manifest[a]) => {
                      val src = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      val func = mirrorLambdaToLmsFunc[a, s](f.asInstanceOf[scalan.Lambda[a, s]], symMirr, funcMirr)
                      val exp = lFunc.sumBy[a, s](src, func)(mA, mS, n.asInstanceOf[Numeric[s]])
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((sym, func)))
                    }
                  }
                }
              }
            }
            case scalan.ParallelExecute(nJobs, sym@scalan.Def(f: scalan.Lambda[_, _])) => {
             f.eB match {
                case el: scalan.Elem[b] => {
                  scalan.createManifest(el) match {
                    case (mB: Manifest[b]) => {
                      val n = symMirr(nJobs).asInstanceOf[lFunc.Exp[Int]]
                      val func = mirrorLambdaToLmsFunc[Int, b](f.asInstanceOf[scalan.Lambda[Int, b]], symMirr, funcMirr)
                      val exp = lFunc.parallelExecute[b](n, func)(mB)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((sym, func)))
                    }
                  }
                }
              }
            }
             /* This is reduce */
            case scalan.ArrayReduce(source, monoid) => {
              source.elem match {
                case el: scalan.ArrayElem[_] => {
                  scalan.createManifest(el.eItem) match {
                    case (mA: Manifest[a]) => {
                      val src = symMirr(source).asInstanceOf[lFunc.Exp[Array[a]]]
                      if (monoid.opName == "+") {
                        val exp = lFunc.sum[a](src)(mA)
                        (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                      } else {
                        monoid.append match {
                          case opSym@scalan.Def(lambda: scalan.Lambda[_, _]) => {
                            val zero = symMirr(monoid.zero).asInstanceOf[lFunc.Exp[a]]
                            val op = mirrorLambdaToLmsFunc[(a, a), a](lambda.asInstanceOf[scalan.Lambda[(a, a), a]], symMirr, funcMirr)
                            val exp = lFunc.reduce[a](src, zero, op)(mA)
                            (exps ++ List(exp), symMirr + ((s, exp)), funcMirr + ((opSym, op)))
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            case scalan.ArrayStride(xs, start, length, stride) =>
              xs.elem match {
                case el: scalan.ArrayElem[a] =>
                  val mA = scalan.createManifest(el.eItem).asInstanceOf[Manifest[a]]
                  val lmsXs = symMirr(xs).asInstanceOf[lFunc.Exp[Array[a]]]
                  val lmsStart = symMirr(start).asInstanceOf[lFunc.Exp[Int]]
                  val lmsLength = symMirr(length).asInstanceOf[lFunc.Exp[Int]]
                  val lmsStride = symMirr(stride).asInstanceOf[lFunc.Exp[Int]]
                  val exp = lFunc.strideArray(lmsXs, lmsStart, lmsLength, lmsStride)(mA)
                  (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
              }
            case scalan.DotSparse(i1, v1, i2, v2) => {
              (v1.elem) match {
                case (el: scalan.ArrayElem[_]) =>
                  (scalan.createManifest(el.eItem)) match {
                    case (mA: Manifest[a]) =>
                      val i1_ = symMirr(i1).asInstanceOf[lFunc.Exp[Array[Int]]]
                      val i2_ = symMirr(i2).asInstanceOf[lFunc.Exp[Array[Int]]]
                      val v1_ = symMirr(v1).asInstanceOf[lFunc.Exp[Array[a]]]
                      val v2_ = symMirr(v2).asInstanceOf[lFunc.Exp[Array[a]]]
                      val exp = lFunc.opDotProductSV[a](i1_, v1_, i2_, v2_)(mA)
                      (exps ++ List(exp), symMirr + ((s, exp)), funcMirr)
                  }
              }
            }

            case _ => scalan.!!!(s"ScalanLMSBridge: Don't know how to mirror symbol ${s.toStringWithDefinition}")
          }
        }
      }
      (lmsExps, finalSymMirr, finalFuncMirr)
    }

    val (lmsExps, finalSymMirror, finalFuncMirror) = mirrorDefs(g, definitions, emptySymMirror, emptyFuncMirror)
    val res = finalFuncMirror(g.roots.last).asInstanceOf[lFunc.Exp[A] => lFunc.Exp[B]](in)

    res
  }

  class LmsFacade[Ctx <: scalan.Transformer](g: scalan.ProgramGraph[Ctx]) extends LmsFunction[A, B] {
    def apply(in: this.Exp[A]): this.Exp[B] = {
      outerApply(this)(in, g)
    }
  }

  def getFacade[Ctx <: scalan.Transformer](g: scalan.ProgramGraph[Ctx]) = new LmsFacade[Ctx](g)
}
