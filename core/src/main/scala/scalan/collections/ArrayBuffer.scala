package scalan.collections

import scalan._
import scalan.common.Default
import scala.reflect.runtime.universe._

trait ArrayBuffers extends Base { self: Scalan =>
  trait ArrayBuffer[T] {
    def apply(i: Rep[Int]): Rep[T] 
    def length : Rep[Int]
    def map[R:Elem](f: Rep[T] => Rep[R]): Rep[ArrayBuffer[R]]
    def update(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]]
    def insert(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]]
    def +=(v: Rep[T]): Rep[ArrayBuffer[T]]
    def ++=(a: Arr[T]): Rep[ArrayBuffer[T]]
    def remove(i: Rep[Int], n: Rep[Int]): Rep[ArrayBuffer[T]]
    def reset():  Rep[ArrayBuffer[T]]     
    def toArray: Arr[T]
  }

  object ArrayBuffer { 
    def apply[T: Elem](v: Rep[T]) = initArrayBuffer[T](v)
    def create[T: Elem](count: Rep[Int], f:Rep[Int]=>Rep[T]) = createArrayBuffer(count, f)
    def make[T](name: Rep[String])(implicit e:Elem[T]) = makeArrayBuffer[T](name)(e)
    def empty[T: Elem] = emptyArrayBuffer[T]
    def fromArray[T: Elem](arr: Arr[T]) = { 
      val buf = empty[T]
      buf ++= arr
    }
  }

  implicit def arrayBufferToArray[T:Elem](buf: Rep[ArrayBuffer[T]]): Arr[T] = buf.toArray
  
  class ArrayBufferElem[T](val eItem: Elem[T]) extends Element[ArrayBuffer[T]] {
    override def isEntityType = eItem.isEntityType

    lazy val tag = {
      implicit val rt = eItem.tag
      implicitly[TypeTag[ArrayBuffer[T]]]
    }

    lazy val defaultRep = Default.defaultVal(emptyArrayBuffer[T](eItem))
  }


  implicit def arrayBufferElement[T](implicit eItem: Elem[T]): ArrayBufferElem[T] = new ArrayBufferElem[T](eItem)
  def extendArrayBufferElement[T](implicit elem: Elem[ArrayBuffer[T]]) = elem.asInstanceOf[ArrayBufferElem[T]]

  implicit def resolveArrayBuffer[T: Elem](buf: Rep[ArrayBuffer[T]]): ArrayBuffer[T]

  def emptyArrayBuffer[T: Elem]: Rep[ArrayBuffer[T]]
  def initArrayBuffer[T: Elem](v: Rep[T]): Rep[ArrayBuffer[T]]
  def makeArrayBuffer[T](name: Rep[String])(implicit e:Elem[T]): Rep[ArrayBuffer[T]]
  def createArrayBuffer[T: Elem](count: Rep[Int], f:Rep[Int=>T]): Rep[ArrayBuffer[T]]
}
  
trait ArrayBuffersSeq extends ArrayBuffers { self: ScalanSeq =>
  implicit class SeqArrayBuffer[T](val impl: scala.collection.mutable.ArrayBuffer[T])(implicit val elem: Elem[T]) extends ArrayBuffer[T] {
    def apply(i: Rep[Int]): Rep[T] = impl.apply(i)
    def length : Rep[Int] = impl.length
    def map[R:Elem](f: Rep[T] => Rep[R]): Rep[ArrayBuffer[R]] = new SeqArrayBuffer[R](impl.map(f))
    def update(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]] = { impl.update(i, v); this }
    def insert(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]] = { impl.insert(i, v); this }
    def +=(v: Rep[T]): Rep[ArrayBuffer[T]] = { impl += v; this }
    def ++=(a: Arr[T]): Rep[ArrayBuffer[T]] = { impl ++= a; this }
    def remove(i: Rep[Int], n: Rep[Int]): Rep[ArrayBuffer[T]] = { impl.remove(i, n); this }
    def reset():  Rep[ArrayBuffer[T]] = {    impl.clear(); this }  
    def toArray: Arr[T] = impl.toArray(elem.classTag)
  }  

  implicit def resolveArrayBuffer[T: Elem](buf: Rep[ArrayBuffer[T]]): ArrayBuffer[T] = buf

  def emptyArrayBuffer[T: Elem]: Rep[ArrayBuffer[T]] = scala.collection.mutable.ArrayBuffer.empty[T]
  def initArrayBuffer[T: Elem](v: Rep[T]): Rep[ArrayBuffer[T]] = scala.collection.mutable.ArrayBuffer(v)
  def makeArrayBuffer[T](name: Rep[String])(implicit e:Elem[T]): Rep[ArrayBuffer[T]] = scala.collection.mutable.ArrayBuffer.empty[T]
  def createArrayBuffer[T: Elem](count: Rep[Int], f:Rep[Int=>T]): Rep[ArrayBuffer[T]] = {
    val buf = scala.collection.mutable.ArrayBuffer.empty[T]
    for (i <- 0 until count) {
      buf += f(i)
    }
    buf
  }
}

trait ArrayBuffersExp extends ArrayBuffers { self: ScalanExp =>
  abstract class ArrayBufferDef[T](implicit val elem: Elem[T]) extends ArrayBuffer[T] with Def[ArrayBuffer[T]] {
    def selfType = element[ArrayBuffer[T]] 
    lazy val uniqueOpId = name(elem)
    
    def apply(i: Rep[Int]): Rep[T] = ArrayBufferApply(this, i)
    def length : Rep[Int] = ArrayBufferLength(this)
    def map[R:Elem](f: Rep[T] => Rep[R]): Rep[ArrayBuffer[R]] = ArrayBufferMap(this, f)
    def update(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]] = ArrayBufferUpdate(this, i, v)
    def insert(i: Rep[Int], v: Rep[T]): Rep[ArrayBuffer[T]] = ArrayBufferInsert(this, i, v)
    def +=(v: Rep[T]): Rep[ArrayBuffer[T]] = ArrayBufferAppend(this, v)
    def ++=(a: Arr[T]): Rep[ArrayBuffer[T]] = ArrayBufferAppendArray(this, a)
    def remove(i: Rep[Int], n: Rep[Int]): Rep[ArrayBuffer[T]] = ArrayBufferRemove(this, i, n)
    def reset():  Rep[ArrayBuffer[T]] = ArrayBufferReset(this)
    def toArray: Arr[T] = ArrayBufferToArray(this)
  }
    
//  def emptyArrayBuffer[T: Elem]: Rep[ArrayBuffer[T]] = ArrayBufferEmpty[T]()
  def emptyArrayBuffer[T: Elem]: Rep[ArrayBuffer[T]] = ArrayBufferUsingFunc(0, fun { i => element[T].defaultRepValue })
  def initArrayBuffer[T: Elem](v: Rep[T]): Rep[ArrayBuffer[T]] = ArrayBufferFromElem(v)
  def makeArrayBuffer[T](name: Rep[String])(implicit e:Elem[T]): Rep[ArrayBuffer[T]] = MakeArrayBuffer(name)(e)
  def createArrayBuffer[T: Elem](count: Rep[Int], f:Rep[Int=>T]): Rep[ArrayBuffer[T]] = ArrayBufferUsingFunc(count, f)

  case class ArrayBufferEmpty[T: Elem]() extends ArrayBufferDef[T] { 
    override def equals(other:Any) = {
      other match {
        case that:ArrayBufferEmpty[_] => (this.selfType equals that.selfType) && super.equals(that)
        case _ => false
      }
    }
    override def mirror(t:Transformer) = ArrayBufferEmpty[T]()
  }

  case class MakeArrayBuffer[T](ctx: Rep[String])(implicit e:Elem[T]) extends ArrayBufferDef[T] {
    override def equals(other:Any) = {
      other match {
        case that:MakeArrayBuffer[_] => (this.selfType equals that.selfType) && (this.ctx equals that.ctx) && super.equals(that)
        case _ => false
      }
    }
    override def mirror(t:Transformer) = MakeArrayBuffer[T](t(ctx))(e)
  }

  case class ArrayBufferFromElem[T: Elem](v: Rep[T]) extends ArrayBufferDef[T] {
    override def mirror(t:Transformer) = ArrayBufferFromElem(t(v))
  }

  case class ArrayBufferUsingFunc[T: Elem](count: Rep[Int], f: Rep[Int=>T]) extends ArrayBufferDef[T] {
    override def mirror(t:Transformer) = ArrayBufferUsingFunc(t(count), t(f))
  }

  case class ArrayBufferApply[T: Elem](buf: Rep[ArrayBuffer[T]], i: Rep[Int]) extends BaseDef[T] {
    override def mirror(t: Transformer) = ArrayBufferApply(t(buf), t(i))
    def uniqueOpId = name(selfType)
  }

  case class ArrayBufferLength[T: Elem](buf: Rep[ArrayBuffer[T]]) extends BaseDef[Int] {
    override def mirror(t: Transformer) = ArrayBufferLength(t(buf))
    def uniqueOpId = name(selfType)
  }

  case class ArrayBufferMap[T: Elem, R:Elem](buf: Rep[ArrayBuffer[T]], f: Rep[T => R]) extends ArrayBufferDef[R] { 
    override def mirror(t:Transformer) = ArrayBufferMap(t(buf), t(f))
  }

  case class ArrayBufferUpdate[T: Elem](buf: Rep[ArrayBuffer[T]], i: Rep[Int], v: Rep[T]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferUpdate(t(buf), t(i), t(v))
  }

  case class ArrayBufferInsert[T: Elem](buf: Rep[ArrayBuffer[T]], i: Rep[Int], v: Rep[T]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferInsert(t(buf), t(i), t(v))
  }

  case class ArrayBufferAppend[T: Elem](buf: Rep[ArrayBuffer[T]], v: Rep[T]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferAppend(t(buf), t(v))
  }

  case class ArrayBufferAppendArray[T: Elem](buf: Rep[ArrayBuffer[T]], a: Arr[T]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferAppendArray(t(buf), t(a))
  }

  case class ArrayBufferRemove[T: Elem](buf: Rep[ArrayBuffer[T]], i: Rep[Int], n: Rep[Int]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferRemove(t(buf), t(i), t(n))
  }

  case class ArrayBufferReset[T: Elem](buf: Rep[ArrayBuffer[T]]) extends ArrayBufferDef[T] { 
    override def mirror(t:Transformer) = ArrayBufferReset(t(buf))
  }

  case class ArrayBufferToArray[T: Elem](buf: Rep[ArrayBuffer[T]]) extends Def[Array[T]] { 
    def selfType = element[Array[T]]
    def uniqueOpId = name(selfType)
    override def mirror(t:Transformer) = ArrayBufferToArray(t(buf))
  }  

  case class ArrayBufferRep[T: Elem](buf: Rep[ArrayBuffer[T]]) extends ArrayBufferDef[T] {
    override def mirror(t: Transformer) = ArrayBufferRep(t(buf))
  }

  implicit def resolveArrayBuffer[T: Elem](sym: Rep[ArrayBuffer[T]]): ArrayBuffer[T] = sym match  {
    case Def(d: ArrayBufferDef[_]) => d.asInstanceOf[ArrayBuffer[T]]
    case s: Exp[_] => {
      val elem = s.elem
      elem match { 
        case ae:ArrayBufferElem[_] => ArrayBufferRep(sym)(ae.asInstanceOf[ArrayBufferElem[T]].eItem)
	case _ => ArrayBufferRep(sym)(elem.asInstanceOf[Elem[T]])
      }
    }
    case _ => ???("cannot resolve ReifiableObject for symbol:", sym)
  }
}