package scalan

import scalan.compilation.GraphVizExport
import scalan.primitives._
import scalan.collections._
import scalan.arrays._
import scalan.seq.BaseSeq
import scalan.staged.{BaseExp, Expressions, Transforming}

trait Scalan
  extends Base
  with Elems
  with Views
  with Proxy
  with Tuples
  with Loops
  with TypeSum
  with UnBinOps
  with LogicalOps
  with OrderingOps
  with NumericOps
  with StringOps
  with Equal
  with MathOps
  with Functions
  with IfThenElse
  with Blocks
  with Monoids
  with Maps
  with ArrayOps
  with ArrayBuffers
  with Exceptions
  with ArrayViews

trait ScalanDsl
  extends Scalan

trait ScalanSeq
  extends Scalan
  with BaseSeq
  with ElemsSeq
  with ViewsSeq
  with ProxySeq
  with TuplesSeq
  with LoopsSeq
  with TypeSumSeq
  with UnBinOpsSeq
  with NumericOpsSeq
  with FunctionsSeq
  with IfThenElseSeq
  with BlocksSeq
  with MapsSeq
  with ArrayOpsSeq
  with ArrayBuffersSeq
  with ExceptionsSeq
  with ArrayViewsSeq

trait ScalanCtxSeq
  extends ScalanDsl
  with ScalanSeq

trait ScalanExp
  extends Scalan
  with BaseExp
  with ElemsExp
  with ViewsExp
  with ProxyExp
  with TuplesExp
  with LoopsExp
  with TypeSumExp
  with NumericOpsExp
  with UnBinOpsExp
  with EqualExp
  with FunctionsExp
  with IfThenElseExp
  with BlocksExp
  with MapsExp
  with ArrayOpsExp
  with ArrayBuffersExp
  with Transforming
  with ExceptionsExp
  with ArrayViewsExp

trait ScalanCtxExp
  extends ScalanDsl
  with ScalanExp
  with Expressions
  with GraphVizExport
